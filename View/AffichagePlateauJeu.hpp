#ifndef AFFICHAGE_PLATEAU
#define AFFICHAGE_PLATEAU

#include <list>
#include "ZoneEcran.hpp"
#include "../Model/Plateau.hpp"
#include "AffichageCase.hpp"
#include "AffichagePacman.hpp"
#include "AffichageFantome.hpp"

class AffichagePlateauJeu : public ZoneEcran{

  Plateau * plateau;
  
  		
  std::vector<std::vector<AffichageCase*> >  tab_affichage_cases; 
  AffichagePacman* affichagePacman;
  std::vector<AffichageFantome*> tabAffichageFantomes;
  
public :
  
  AffichagePlateauJeu(int largeur, int hauteur, int posX, int posY, Plateau * plateau);
  

  void loadTabTextureJeu();
  void afficher(SDL_Renderer* renderer);

  void resetAffichagePlateau();
 ~AffichagePlateauJeu();
};

#endif
