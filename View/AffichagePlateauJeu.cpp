#include <iostream>
#include "AffichagePlateauJeu.hpp"

using namespace std;

AffichagePlateauJeu::AffichagePlateauJeu(int largeur, int hauteur, int posX, int posY, Plateau * plateau)
  : ZoneEcran(largeur, hauteur, posX, posY){
  this->plateau = plateau;
  loadTabTextureJeu();
  
}

void AffichagePlateauJeu::resetAffichagePlateau(){
  for(int i = 0;i< tab_affichage_cases.size(); i++){
    for (int j = 0; j < tab_affichage_cases[i].size(); j++){
      delete tab_affichage_cases[i][j];
    }
    tab_affichage_cases[i].clear();
  }
  tab_affichage_cases.clear();

  cout<<"Delete Pacman"<<endl;
  delete affichagePacman;

  loadTabTextureJeu();
  
}




void AffichagePlateauJeu::loadTabTextureJeu(){
  /*
  int largeur_case = largeur / plateau->getLargeur();
  int hauteur_case = hauteur / plateau->getHauteur();
  taille_case = min(largeur_case, hauteur_case);
  */

  int taille_case = plateau->getTailleCase();
  for (int i = 0; i < plateau->getHauteur(); i++){
    tab_affichage_cases.push_back(vector<AffichageCase*>());
    for (int j = 0; j < plateau->getLargeur(); j++){
      tab_affichage_cases[i].push_back(new AffichageCase( posX + i*taille_case, posY + j*taille_case, taille_case, taille_case, plateau->getCase(i, j)));
    }
	   
  }
 
  if(plateau->getPacman()){
    plateau->getPacman()->calculPos();
    affichagePacman = new AffichagePacman(posX,posY, taille_case, taille_case, plateau->getPacman());
  }
    else{
      affichagePacman = NULL;
    }
  for (int i = 0; i < plateau->getFantomes().size(); i++){
    plateau->getFantomes()[i]->calculPos();
    tabAffichageFantomes.push_back(new AffichageFantome( posX, posY, taille_case, taille_case, plateau->getFantomes()[i]));
  }
}

void AffichagePlateauJeu::afficher(SDL_Renderer * renderer){
  for (int i = 0; i < tab_affichage_cases.size(); i++){
    for (int j = 0; j < tab_affichage_cases[i].size(); j++){
      tab_affichage_cases[i][j]->afficher(renderer);
    }
  }

  if(affichagePacman)
    affichagePacman->afficher(renderer);
  		
  for (int i = 0; i < tabAffichageFantomes.size(); i++){
    tabAffichageFantomes[i]->afficher(renderer);
    }
}


AffichagePlateauJeu::~AffichagePlateauJeu(){
  //cout<<"Delete Pacman"<<endl;
  delete affichagePacman;
  
  for(int i = 0;i<tab_affichage_cases.size(); i++)
    for (int j = 0; j < tab_affichage_cases[i].size(); j++){

      delete tab_affichage_cases[i][j];
    }
  
  for(int i = 0; i < tabAffichageFantomes.size();i++)
    delete tabAffichageFantomes[i];
}


      

