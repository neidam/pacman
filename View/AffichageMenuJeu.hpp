#ifndef AFFICHAGE_MENU_JEU
#define AFFICHAGE_MENU_JEU

#include <string>
#include <sstream>
#include <utility>
#include <vector>

#include "AffichageBouton.hpp"
#include "AffichageObjet.hpp"
#include "AffichageTexte.hpp"
#include "AffichageViePacman.hpp"
#include "../Model/PartieStandard.hpp"
#include "ZoneEcran.hpp"

class AffichageMenuJeu : public ZoneEcran {

  PartieStandard * partieStandard;
  AffichageTexte * affichageScore;
  AffichageViePacman * affichageViePacman;
  AffichageTexte * affichagePerdu;
  AffichageTexte * affichageGagne;
  AffichageImage * affichageFruit;
  
  
  std::vector< std::pair<TypeBouton, AffichageBouton *> >boutons;
  
  TTF_Font * police;
  
public:
  AffichageMenuJeu(int largeur, int hauteur, int posX, int posY, PartieStandard * partieStandard);

  void afficher(SDL_Renderer * renderer);
  void chargerPolice();
  void update();
  std::string intToString(int i);
  std::vector< std::pair<TypeBouton, AffichageBouton* > > getBoutons();
  ~AffichageMenuJeu();
};
#endif
