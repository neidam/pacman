#ifndef AFFICHAGE_CLASSE
#define AFFICHAGE_CLASSE

#include <iostream>
#include <map>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "../Global.hpp"

class AffichageClasse{
	
	public:		
	
		typedef std::map<int, SDL_Texture*> mapTexture;

  AffichageClasse();
		AffichageClasse(int largeur, int hauteur);
		virtual ~AffichageClasse();
	
		SDL_Texture* chargerTexture(std::string src, SDL_Renderer* renderer);
  //virtual void ajouterTexture(std::string src, int type, SDL_Renderer* renderer) = 0;
		virtual void afficher(SDL_Renderer* renderer) = 0;
  
  // static SDL_Texture * getTexture(int type)=0;  
		
	protected:
		
		SDL_Rect* rect;
		
		static mapTexture init_map(){
			mapTexture newMap;
			return newMap;
		}
		
	
};

#endif
