#include "AffichageBoutonEdition.hpp"

using namespace std;

AffichageClasse::mapTexture	AffichageBoutonEdition::textures = init_map();


AffichageBoutonEdition::AffichageBoutonEdition()
  : AffichageClasse(){
  affichageImage = NULL;
}

AffichageBoutonEdition::AffichageBoutonEdition(int largeur, int hauteur, int posX, int posY, TypeImageEdition type)
  : AffichageClasse(largeur, hauteur){
  
  affichageImage = new AffichageImage(largeur, hauteur, posX, posY, textures[type]);
  this->type = type;
  
}

TypeImageEdition AffichageBoutonEdition::getType(){
  return type;
}

void AffichageBoutonEdition::afficher(SDL_Renderer* renderer){
  if(affichageImage) affichageImage->afficher(renderer);
}

void AffichageBoutonEdition::ajouterTexture(string src, int type, SDL_Renderer* renderer){
	SDL_Texture* newTexture = chargerTexture(src, renderer);
	textures[type] = newTexture;
}

void AffichageBoutonEdition::chargerToutesTextures(SDL_Renderer * renderer){
  AffichageBoutonEdition aff;
  aff.ajouterTexture("Ressources/Images/bouton_plus.png", imagePlus, renderer);
  aff.ajouterTexture("Ressources/Images/bouton_moins.png", imageMoins, renderer);
  
}


void AffichageBoutonEdition::supprimerToutesTextures(){
  textures.clear();
}

AffichageBoutonEdition::~AffichageBoutonEdition(){
  if(affichageImage) delete affichageImage;
}
