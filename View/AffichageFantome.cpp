#include "AffichageFantome.hpp"

using namespace std;

AffichageClasse::mapTexture	AffichageFantome::textures = init_map();

AffichageFantome::AffichageFantome(){}

AffichageFantome::AffichageFantome(int posX, int posY, int largeur, int hauteur, Fantome* fantome)
  : AffichageClasse(largeur, hauteur)
{	
	this->fantome = fantome;
	this->posX = posX;
	this->posY = posY;
}

AffichageFantome::~AffichageFantome(){
}

void AffichageFantome::ajouterTexture(string src, int type, SDL_Renderer* renderer){
	SDL_Texture* newTexture = chargerTexture(src, renderer);
	textures[type] = newTexture;
}

void AffichageFantome::afficher(SDL_Renderer* renderer){
	// Inversion des pos obligatoire
	rect->x =  posY + fantome->getPosX();
	rect->y = posX + fantome->getPosY();
	
	if (fantome->getEtat() == poursuite || fantome->getEtat() == attente ||fantome->getEtat() == sortMaison)
		SDL_RenderCopy(renderer, textures[fantome->getNomFantome()], NULL, rect);
	else if (fantome->getEtat() == fuite || fantome->getEtat() == retourMaison) 
		SDL_RenderCopy(renderer, textures[fantome->getEtat()], NULL, rect);
}

void AffichageFantome::chargerToutesTextures(SDL_Renderer * renderer){	
  AffichageFantome aff;
  aff.ajouterTexture("Ressources/Images/pinky.png", pinky, renderer);
  aff.ajouterTexture("Ressources/Images/clyde.png", clyde, renderer);
  aff.ajouterTexture("Ressources/Images/inky.png", inky, renderer);
  aff.ajouterTexture("Ressources/Images/blinky.png", blinky, renderer);
  aff.ajouterTexture("Ressources/Images/fantomeFuite.png", fuite, renderer);
  aff.ajouterTexture("Ressources/Images/retourMaison.png", retourMaison, renderer);
}

void AffichageFantome::supprimerToutesTextures(){
  textures.clear();
}
