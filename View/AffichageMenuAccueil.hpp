#ifndef AFFICHAGE_MENU_ACCUEIL
#define AFFICHAGE_MENU_ACCUEIL

#include <string>
#include <sstream>
#include <utility>
#include <vector>

#include "AffichageBouton.hpp"
#include "AffichageTexte.hpp"
#include "ZoneEcran.hpp"


class AffichageMenuAccueil : public ZoneEcran{

  AffichageTexte * affichageMenu;

  std::vector< std::pair<TypeBouton, AffichageBouton *> >boutons;

  TTF_Font * police;

public:
  AffichageMenuAccueil(int largeur, int hauteur, int posX, int posY);

  void afficher(SDL_Renderer * renderer);

  void chargerPolice();

  std::string intToString(int i);

  std::vector< std::pair<TypeBouton, AffichageBouton* > > getBoutons();

  ~AffichageMenuAccueil();
  
};

#endif
