#ifndef AFFICHAGE_PARTIE
#define AFFICHAGE_PARTIE

#include "AffichagePlateauJeu.hpp"
#include "AffichageMenuJeu.hpp"
#include "../Controller/ControllerPartie.hpp"
#include "../Model/PartieStandard.hpp"
#include "ZoneEcran.hpp"

class AffichagePartie : public ZoneEcran{

  PartieStandard * partieStandard;
  AffichagePlateauJeu * affichagePlateauJeu;
  AffichageMenuJeu * affichageMenuJeu;
  ControllerPartie * controllerPartie;

public :
  AffichagePartie(int largeur, int hauteur, int posX, int posY);
  
  void afficher(SDL_Renderer * renderer);

  void genererAffichageNiveauSuivant();
  ~AffichagePartie();

};



#endif
