#ifndef AFFICHAGE_BOUTON_EDITION
#define AFFICHAGE_BOUTON_EDITION

#include <list>
#include "AffichageImage.hpp"
#include "AffichageClasse.hpp"

enum TypeImageEdition{imagePlus, imageMoins};

class AffichageBoutonEdition : public AffichageClasse{

  	
  static mapTexture textures;
  AffichageImage * affichageImage;
  TypeImageEdition type;

public:

  AffichageBoutonEdition();
  AffichageBoutonEdition(int largeur, int hauteur, int posX, int posY, TypeImageEdition type);
  
  void afficher(SDL_Renderer* renderer);

  void ajouterTexture(std::string src, int type, SDL_Renderer* renderer);
  static void chargerToutesTextures(SDL_Renderer * renderer);

  TypeImageEdition getType();
  void supprimerToutesTextures();

  ~AffichageBoutonEdition();
};

#endif
