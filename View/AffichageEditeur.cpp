#include <iostream>

#include "AffichageEditeur.hpp"

using namespace std;

AffichageEditeur::AffichageEditeur(int largeur, int hauteur, int posX, int posY)
  : ZoneEcran(largeur, hauteur, posX, posY)
{

  editeur = new Editeur();

  affichageZoneEdition = new AffichageZoneEdition(3 * largeur/4, hauteur, posX, posY + largeur/4, editeur->getPlateau());
  affichageMenuEditeur = new AffichageMenuEditeur(largeur/4, hauteur, posX, posY);
  
  controllerEditeur = new ControllerEditeur(editeur, affichageZoneEdition->getAffichageImageLargeur(), affichageZoneEdition->getAffichageImageHauteur());
  
  
  for(int i = 0; i<affichageMenuEditeur->getBoutons().size();i++){
    controllerEditeur->ajouterAffichageBouton(affichageMenuEditeur->getBoutons()[i].first, affichageMenuEditeur->getBoutons()[i].second);
    
  }
}

void AffichageEditeur::afficher(SDL_Renderer * renderer){
  
  // Gestion des évènements
  controllerEditeur->gererEvenements();
  if(controllerEditeur->getModif()){
    affichageZoneEdition->resetAffichagePlateau(); // A Optimiser
    controllerEditeur->resetModif(); 
    controllerEditeur->setAffichageBoutonsEdition(affichageZoneEdition->getAffichageImageLargeur(), affichageZoneEdition->getAffichageImageHauteur());
  }
  if(controllerEditeur->getEtatEditeur()==sauvegarder)
    {
      editeur->sauvegarderPlateau();
      controllerEditeur->resetEtatEditeur();
    }
  affichageZoneEdition->afficher(renderer);  
  affichageMenuEditeur->afficher(renderer);
  
  
  
}

AffichageEditeur::~AffichageEditeur(){
  delete controllerEditeur;
  delete affichageMenuEditeur;
  delete affichageZoneEdition;
  delete editeur;
}
