#include "AffichageObjet.hpp"
#include <iostream>
using namespace std;

AffichageClasse::mapTexture AffichageObjet::textures = init_map();

AffichageObjet::AffichageObjet(){}

AffichageObjet::AffichageObjet(int pos_x, int pos_y, int largeur, int hauteur, Case * passage)
  : AffichageClasse(largeur, hauteur)
{
  this->passage = passage;
  this->rect->x = pos_y;
  this->rect->y = pos_x;
}

AffichageObjet::~AffichageObjet(){
  
}

void AffichageObjet::ajouterTexture(string src, int type, SDL_Renderer* renderer){
	SDL_Texture* newTexture = chargerTexture(src, renderer);
	textures[type] = newTexture;
}

void AffichageObjet::afficher(SDL_Renderer* renderer){
  if(passage->getContenant()!=NULL)
    SDL_RenderCopyEx(renderer, textures[passage->getContenant()->getType()], NULL, rect, 0, NULL, SDL_FLIP_NONE);
  
}

void AffichageObjet::chargerToutesTextures(SDL_Renderer * renderer){
  AffichageObjet aff;

  aff.ajouterTexture("Ressources/Images/bonus.png", bonus, renderer);
  aff.ajouterTexture("Ressources/Images/pacgum.png", pacgum, renderer);
  aff.ajouterTexture("Ressources/Images/cerise.png", cerise, renderer);
  aff.ajouterTexture("Ressources/Images/fraise.png", fraise, renderer);
  aff.ajouterTexture("Ressources/Images/orange.png", orange, renderer);
  aff.ajouterTexture("Ressources/Images/pomme.png", pomme, renderer);
  aff.ajouterTexture("Ressources/Images/melon.png", melon, renderer);
  aff.ajouterTexture("Ressources/Images/galboss.png", galboss, renderer);
  aff.ajouterTexture("Ressources/Images/cloche.png", cloche, renderer);
  aff.ajouterTexture("Ressources/Images/cle.png", cle, renderer);
}

void AffichageObjet::supprimerToutesTextures(){
  textures.clear();
}

SDL_Texture* AffichageObjet::getTexture(int type){
  return textures[type];
}
