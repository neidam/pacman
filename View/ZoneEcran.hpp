#ifndef ZONE_ECRAN
#define ZONE_ECRAN

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <vector>

class ZoneEcran{

protected :
  int largeur, hauteur;
  int posX, posY;

public : 
  
  ZoneEcran();
  ZoneEcran(int largeur, int hauteur, int posX, int posY);

  virtual void afficher(SDL_Renderer *renderer)=0;
  virtual ~ZoneEcran();
};

#endif
