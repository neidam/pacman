#ifndef AFFICHAGE_IMAGE
#define AFFICHAGE_IMAGE

#include "AffichageClasse.hpp"


class AffichageImage : public AffichageClasse{

  SDL_Texture * texture;
  
public:
  AffichageImage();
  AffichageImage(int largeur, int hauteur, int posX, int posY, SDL_Texture * tecture);

  void afficher(SDL_Renderer * renderer);
  
  void setTexture(SDL_Texture * texture);

  ~AffichageImage();
};

#endif
