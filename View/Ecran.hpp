#ifndef ECRAN
#define ECRAN

#include <list>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>

#include "AffichageAccueil.hpp"
#include "AffichageBouton.hpp"
#include "AffichageCase.hpp"
#include "AffichageEditeur.hpp"
#include "AffichageFantome.hpp"
#include "AffichagePacman.hpp"
#include "AffichagePartie.hpp"
#include "../Controller/Controller.hpp"
#include "../Global.hpp"
#include "../Model/PartieStandard.hpp"
#include "ZoneEcran.hpp"



class Ecran{
	
private:

  int largeur, hauteur;
  SDL_Window *fenetre;	
  SDL_Renderer* renderer;
  ZoneEcran * zoneEcran;
  Controller * controller;
		
  EtatAccueil etat;
  
public:
	
  Ecran(int largeur, int hauteur);
  ~Ecran();
		
  void afficher();
 
  void InitSDL();

  void testEtatProgramme();

  
  void loadTextures();

  void deleteTextures();
	
};

#endif
