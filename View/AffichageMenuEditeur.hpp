#ifndef AFFICHAGE_MENU_EDITEUR
#define AFFICHAGE_MENU_EDITEUR

#include <string>
#include <sstream>
#include <utility>
#include <vector>

#include "AffichageBouton.hpp"
#include "AffichageTexte.hpp"
#include "ZoneEcran.hpp"


class AffichageMenuEditeur : public ZoneEcran{

  AffichageTexte * affichageTitre;
  
  std::vector< std::pair<TypeBouton, AffichageBouton *> >boutons;

  TTF_Font * police;

public:
  AffichageMenuEditeur(int largeur, int hauteur, int posX, int posY);

  void afficher(SDL_Renderer * renderer);

  void chargerPolice();

  std::string intToString(int i);

  std::vector< std::pair<TypeBouton, AffichageBouton* > > getBoutons();

  ~AffichageMenuEditeur();
  
};

#endif
