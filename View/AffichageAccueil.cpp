#include <iostream>

#include "AffichageAccueil.hpp"

using namespace std;

AffichageAccueil::AffichageAccueil(int largeur, int hauteur, int posX, int posY)
  : ZoneEcran(largeur, hauteur, posX, posY)
{



  accueil = new Accueil();
 
  affichageMenuAccueil = new AffichageMenuAccueil(largeur/4, hauteur, posX, posY);
  affichagePlateauJeu = new AffichagePlateauJeu(3 * largeur/4, hauteur, posX, posY + largeur/4, accueil->getPlateau());
  controllerAccueil = new ControllerAccueil(accueil);
  
  for(int i = 0; i<affichageMenuAccueil->getBoutons().size();i++)
    controllerAccueil->ajouterAffichageBouton(affichageMenuAccueil->getBoutons()[i].first, affichageMenuAccueil->getBoutons()[i].second);
  
}

void AffichageAccueil::afficher(SDL_Renderer * renderer){

  // Gestion des évènements
  controllerAccueil->gererEvenements();
  
  accueil->update();
  
  affichagePlateauJeu->afficher(renderer); 
  
  affichageMenuAccueil->afficher(renderer);
  
}



AffichageAccueil::~AffichageAccueil(){
  delete affichageMenuAccueil;
  delete affichagePlateauJeu;
  delete controllerAccueil;
  delete accueil;
}
