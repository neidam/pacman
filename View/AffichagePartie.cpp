#include <iostream>

#include "AffichagePartie.hpp"

using namespace std;

AffichagePartie::AffichagePartie(int largeur, int hauteur, int posX, int posY)
  : ZoneEcran(largeur, hauteur, posX, posY)
{

  
  //partieStandard = new PartieStandard("Ressources/Levels/levelJeu3");
  partieStandard = new PartieStandard("OriginalMap.txt");

  affichagePlateauJeu = new AffichagePlateauJeu(3 * largeur/4, hauteur, posX, posY + largeur/4, partieStandard->getPlateau());
  affichageMenuJeu = new AffichageMenuJeu(largeur/4, hauteur, posX, posY, partieStandard);
  
  controllerPartie = new ControllerPartie(partieStandard);
  
  
  for(int i = 0; i<affichageMenuJeu->getBoutons().size();i++)
    controllerPartie->ajouterAffichageBouton(affichageMenuJeu->getBoutons()[i].first, affichageMenuJeu->getBoutons()[i].second);
  
  
}

void AffichagePartie::afficher(SDL_Renderer * renderer){
  
 // Gestion des évènements
  controllerPartie->gererEvenements();
  
  if(partieStandard->getEtat()!=fini){
    
    // Maj du modèle
    if(partieStandard->getEtat()==en_cours)
      partieStandard->update();
    
    if(partieStandard->getEtat()==gagne){
      partieStandard->niveauSuivant();
      genererAffichageNiveauSuivant();
    }
    affichagePlateauJeu->afficher(renderer);  
    affichageMenuJeu->afficher(renderer);
  }
  
  
}

void AffichagePartie::genererAffichageNiveauSuivant(){
  delete affichagePlateauJeu;


  affichagePlateauJeu = new AffichagePlateauJeu(3 * largeur/4, hauteur, posX, posY + largeur/4, partieStandard->getPlateau());
  
  delete affichageMenuJeu;

  affichageMenuJeu = new AffichageMenuJeu(largeur/4, hauteur, posX, posY, partieStandard);
  
  delete controllerPartie;
  controllerPartie = new ControllerPartie(partieStandard);
  
  for(int i = 0; i<affichageMenuJeu->getBoutons().size();i++)
    controllerPartie->ajouterAffichageBouton(affichageMenuJeu->getBoutons()[i].first, affichageMenuJeu->getBoutons()[i].second);
  
  partieStandard->setEtat(pause);
}

AffichagePartie::~AffichagePartie(){
  delete controllerPartie;
  delete affichageMenuJeu;
  delete affichagePlateauJeu;
  delete partieStandard;
}
