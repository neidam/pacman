#include "AffichageCase.hpp"
#include <iostream>

using namespace std;

AffichageClasse::mapTexture AffichageCase::textures = init_map();

AffichageCase::AffichageCase(){
  affichageObjet = NULL;
}

AffichageCase::AffichageCase(int pos_x, int pos_y, int largeur, int hauteur, Case *c)
  : AffichageClasse(largeur, hauteur)
{
  this->c = c;
  if(c->getType()==passage)
    affichageObjet = new AffichageObjet(pos_x, pos_y, largeur, hauteur, c);
  else
    affichageObjet = NULL;
  
  this->rect->x = pos_y;
  this->rect->y = pos_x;

}

AffichageCase::~AffichageCase(){
 
  if(affichageObjet)
    delete affichageObjet;
}

void AffichageCase::ajouterTexture(string src, int type, SDL_Renderer* renderer){
	SDL_Texture* newTexture = chargerTexture(src, renderer);
	textures[type] = newTexture;
}

void AffichageCase::afficher(SDL_Renderer* renderer){
  SDL_RenderCopyEx(renderer, textures[c->getType()], NULL, rect, c->getAngle(), NULL, SDL_FLIP_NONE);

  if(affichageObjet!=NULL)
    affichageObjet -> afficher(renderer);
  
    
  
  /* Permet de mettre un quadrillage*/
  /*
  SDL_SetRenderDrawColor(renderer, 255,0,0,255);
  SDL_RenderDrawLine(renderer, rect->x, rect->y, rect->x, rect->y+rect->h);
  SDL_RenderDrawLine(renderer, rect->x, rect->y, rect->x+rect->w, rect->y);
  SDL_SetRenderDrawColor(renderer, 0,0,0,255);
  */
}


void AffichageCase::chargerToutesTextures(SDL_Renderer * renderer){
  AffichageCase aff;
  aff.ajouterTexture("Ressources/Images/mur_ligne_plein.bmp", ligne_plein, renderer);
  aff.ajouterTexture("Ressources/Images/mur_ligne_vide.bmp", ligne_vide, renderer);
  aff.ajouterTexture("Ressources/Images/mur_coin_plein.bmp", coin_plein, renderer);
  aff.ajouterTexture("Ressources/Images/mur_coin_vide.bmp", coin_vide, renderer);
  aff.ajouterTexture("Ressources/Images/mur_coin_complet.bmp", coin_complet, renderer);
  aff.ajouterTexture("Ressources/Images/mur_pic.bmp", pic, renderer);
  aff.ajouterTexture("Ressources/Images/mur_seul.bmp", seul, renderer);
  aff.ajouterTexture("Ressources/Images/mur_double_coin_vide.bmp", double_coin_vide, renderer);
  aff.ajouterTexture("Ressources/Images/mur_double_coin_plein.bmp", double_coin_plein, renderer);
  aff.ajouterTexture("Ressources/Images/mur_croix.bmp", croix, renderer);
  aff.ajouterTexture("Ressources/Images/mur_sablier.bmp", sablier, renderer);
  aff.ajouterTexture("Ressources/Images/mur_trois_coins.bmp", trois_coins, renderer);
  aff.ajouterTexture("Ressources/Images/mur_double_coin_imcomplet.bmp", double_coin_imcomplet,  renderer);
  aff.ajouterTexture("Ressources/Images/mur_double_coin_imcomplet_bis.bmp", double_coin_imcomplet_bis, renderer);
  aff.ajouterTexture("Ressources/Images/porteFantomes.bmp", porteFantomes, renderer);
}

void AffichageCase::supprimerToutesTextures(){
  textures.clear();
}
