#include <iostream>
#include <string>
#include "AffichageMenuAccueil.hpp"

using namespace std;

AffichageMenuAccueil::AffichageMenuAccueil(int largeur, int hauteur, int posX, int posY)
  : ZoneEcran(largeur, hauteur, posX, posY)
{

  chargerPolice();
  affichageMenu = new AffichageTexte(4*largeur/5, hauteur/20, posX+5, posY+10, police, "Menu");
  
  boutons.push_back(pair<TypeBouton, AffichageBouton*> (boutonJouer, new AffichageBouton(4*largeur/5, hauteur/10, posX + (10 + hauteur/20 )*4, posY+10, police, "Jouer", normal)));
  
boutons.push_back(pair<TypeBouton, AffichageBouton*> (boutonEditeur, new AffichageBouton(4*largeur/5, hauteur/10, posX + (10 + hauteur/20 )*9, posY+10, police, "Editeur", normal)));
  
}

void AffichageMenuAccueil::afficher(SDL_Renderer * renderer){
  affichageMenu->afficher(renderer);
  for(int i = 0; i <boutons.size();i++)
    boutons[i].second->afficher(renderer);
  
}

void AffichageMenuAccueil::chargerPolice(){
  police = TTF_OpenFont("Ressources/Fonts/alterebro-pixel-font.ttf", 30);
  if(!police)
    printf("TTF_OpenFront: %s\n", TTF_GetError()); 
}


vector<pair<TypeBouton, AffichageBouton *> > AffichageMenuAccueil::getBoutons(){
  return boutons;
}


AffichageMenuAccueil::~AffichageMenuAccueil(){
  delete affichageMenu;
  
  if(police!=NULL)
    TTF_CloseFont(police);
  police=NULL;
 
  for(int i = 0; i <boutons.size();i++)
    delete boutons[i].second;
  
}

string AffichageMenuAccueil::intToString(int i){
  stringstream out;
  out<<i;
  return out.str();
}
