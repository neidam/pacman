#include <iostream>
#include "AffichageTexte.hpp"

using namespace std;

AffichageTexte::AffichageTexte()
  : AffichageClasse()
{}


AffichageTexte::AffichageTexte(int largeur, int hauteur, int posX, int posY, TTF_Font * police, string texte)
  : AffichageClasse(largeur, hauteur)
{
  this->texte = texte;
  this->police = police;
  rect->x = posY;
  rect->y = posX;
  rect->w = largeur;
  rect->h = hauteur;
  
}

void AffichageTexte::afficher(SDL_Renderer * renderer){
  SDL_Color color = {255, 255, 255};
  SDL_Surface * textSurface;
  if(!(textSurface = TTF_RenderText_Solid(police, texte.c_str(), color)))
    cout<<"ERROR CODE"<<endl;
  else {
    SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, textSurface);
    //SDL_QueryTexture(texture, NULL, NULL, &rectText->h, &rectText->w);
    SDL_RenderCopy(renderer, texture, NULL, rect);
    SDL_DestroyTexture(texture); 
  }
  SDL_FreeSurface(textSurface);
}

void AffichageTexte::setTexte(string texte){
  this->texte = texte;
}

AffichageTexte::~AffichageTexte(){
  
}
