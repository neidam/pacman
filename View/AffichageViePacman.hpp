#ifndef AFFICHAGE_VIE_PACMAN
#define AFFICHAGE_VIE_PACMAN

#include <list>
#include "AffichagePacman.hpp"
#include "AffichageImage.hpp"
#include "../Model/PartieStandard.hpp"
#include "ZoneEcran.hpp"


class AffichageViePacman : public ZoneEcran{

  std::vector<AffichageImage * > listeVie;  
  int viePacman;

public : 
  AffichageViePacman(int largeur, int hauteur, int posX, int posY, PartieStandard* partieStandard);

  void setViePacman(int vie);
  
  void afficher(SDL_Renderer * renderer);
  
  ~AffichageViePacman();
};

#endif
