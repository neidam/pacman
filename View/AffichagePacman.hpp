#ifndef AFFICHAGE_PACMAN
#define AFFICHAGE_PACMAN

#include "AffichageClasse.hpp"
#include "../Model/Entite.hpp"

enum TypeTexturePacman{
  pacmanOuvert,
  pacmanFerme
};

class AffichagePacman : public AffichageClasse{
	
public:
  
  AffichagePacman();
  AffichagePacman(int posX, int posY, int largeur, int hauteur, Entite* pacman);
  ~AffichagePacman();
		
  void afficher(SDL_Renderer* renderer);
  void ajouterTexture(std::string src, int type, SDL_Renderer* renderer);
  static void chargerToutesTextures(SDL_Renderer * renderer);

  void supprimerToutesTextures();
  
  static SDL_Texture * getTexture(int type);
	
private:
	
  static mapTexture textures;
  Entite* pacman;
  int framerateImage;	
  int posX, posY;
	
};

#endif
