#ifndef AFFICHAGE_FANTOME
#define AFFICHAGE_FANTOME

#include "AffichageClasse.hpp"
#include "../Model/Fantome.hpp"


class AffichageFantome : public AffichageClasse{
	
	public:
 
		AffichageFantome();
  AffichageFantome(int posX, int posY, int largeur, int hauteur, Fantome* fantome);
		~AffichageFantome();
		
		void afficher(SDL_Renderer* renderer);
		void ajouterTexture(std::string src, int type, SDL_Renderer* renderer);
		static void chargerToutesTextures(SDL_Renderer * renderer);

  void supprimerToutesTextures();
  //static SDL_Texture * getTexture(int type);
	private:
	
		static mapTexture textures;
		Fantome* fantome;
  int posX, posY;
};


#endif
