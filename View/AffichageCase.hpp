#ifndef AFFICHAGE_CASE
#define AFFICHAGE_CASE

#include <SDL2/SDL.h>

#include "AffichageClasse.hpp"
#include "AffichageObjet.hpp"
#include "../Model/Case.hpp"

class AffichageCase : public AffichageClasse{
	
private:

  Case * c;
  AffichageObjet * affichageObjet;
  static mapTexture textures;
  
		
public:

  AffichageCase();
  
  AffichageCase(int pos_x, int pos_y, int largeur, int hauteur, Case* c);
  ~AffichageCase();

  void ajouterTexture(std::string src,int type, SDL_Renderer* renderer);
		
  void afficher(SDL_Renderer* renderer);

  static void chargerToutesTextures(SDL_Renderer * renderer);
  void supprimerToutesTextures();
};

#endif
