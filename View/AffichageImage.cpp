#include <iostream>
#include "AffichageImage.hpp"

using namespace std;

AffichageImage::AffichageImage()
  : AffichageClasse()
{}


AffichageImage::AffichageImage(int largeur, int hauteur, int posX, int posY, SDL_Texture * texture)
  : AffichageClasse(largeur, hauteur)
{
  this->texture = texture;
  rect->x = posY;
  rect->y = posX;
  rect->w = largeur;
  rect->h = hauteur;
  
}

void AffichageImage::setTexture(SDL_Texture * texture){
  this->texture = texture;
}

void AffichageImage::afficher(SDL_Renderer * renderer){
  SDL_RenderCopy(renderer, texture, NULL, rect);
}
AffichageImage::~AffichageImage(){
 
}
