#ifndef AFFICHAGE_ACCUEIL
#define AFFICHAGE_ACCUEIL

#include "../Model/Accueil.hpp"
#include "AffichageMenuAccueil.hpp"
#include "AffichagePlateauJeu.hpp"
#include "../Controller/ControllerAccueil.hpp"
#include "ZoneEcran.hpp"

class AffichageAccueil : public ZoneEcran{

  Accueil * accueil;
  AffichageMenuAccueil * affichageMenuAccueil;
  AffichagePlateauJeu * affichagePlateauJeu;
  ControllerAccueil * controllerAccueil;

public: 
  AffichageAccueil(int largeur, int hauteur, int posX, int posY);

  void afficher(SDL_Renderer * renderer);

  ~AffichageAccueil();
};

#endif
