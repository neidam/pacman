#include <iostream>

#include "AffichageBouton.hpp"

using namespace std;

AffichageClasse::mapTexture	AffichageBouton::textures = init_map();

AffichageBouton::AffichageBouton()
  : AffichageClasse()
{
  affichageImage = NULL;
   affichageTexte = NULL;
}


AffichageBouton::AffichageBouton(int largeur, int hauteur, int posX, int posY, TTF_Font * police, string texte, EtatBouton etatBouton)
  : AffichageClasse(largeur, hauteur)
{
  this->police = police;
  rect->x = posY;
  rect->y = posX;
  rect->w = largeur;
  rect->h = hauteur;
  this->etatBouton = etatBouton;
  
  affichageImage = new AffichageImage(largeur, hauteur, posX, posY, textures[etatBouton]);
  affichageTexte = new AffichageTexte(largeur/2, hauteur/2, posX + hauteur/5, posY+largeur/5, police, texte);
}

EtatBouton AffichageBouton::getEtat(){
  return etatBouton;
}

void AffichageBouton::setEtat(EtatBouton etat){
  this->etatBouton = etat;
  affichageImage->setTexture(textures[etat]);
}

void AffichageBouton::afficher(SDL_Renderer * renderer){
  affichageImage->afficher(renderer);
  affichageTexte->afficher(renderer);
}

void AffichageBouton::ajouterTexture(string src, int type, SDL_Renderer* renderer){
	SDL_Texture* newTexture = chargerTexture(src, renderer);
	textures[type] = newTexture;
}

void AffichageBouton::chargerToutesTextures(SDL_Renderer * renderer){	
  AffichageBouton aff;
  aff.ajouterTexture("Ressources/Images/bouton_normal.bmp", normal, renderer);
  aff.ajouterTexture("Ressources/Images/bouton_survol.bmp", survol, renderer);
  aff.ajouterTexture("Ressources/Images/bouton_enfonce.bmp", enfonce, renderer);
}

bool AffichageBouton::inBouton(int x, int y){
  return (( x>=rect->x && x<rect->x + rect->w) && (y>=rect->y && y <=rect->y + rect->h));
}

void AffichageBouton::supprimerToutesTextures(){
  textures.clear();
}

AffichageBouton::~AffichageBouton(){
  if(affichageImage) delete affichageImage;
  if(affichageTexte) delete affichageTexte;
}


