#include <iostream>
#include <string>
#include "AffichageMenuJeu.hpp"

using namespace std;


AffichageMenuJeu::AffichageMenuJeu(int largeur, int hauteur, int posX, int posY, PartieStandard * partieStandard)
: ZoneEcran(largeur, hauteur, posX, posY)
{
  this->partieStandard = partieStandard;
  chargerPolice();
  affichageScore = new AffichageTexte(4*largeur/5, hauteur/20, posX+5, posY+10, police, "Score : 0");
  
  affichageViePacman = new AffichageViePacman(4*largeur/5, hauteur/20, posX + (10+hauteur/20) , posY+10, partieStandard);
  affichagePerdu = new AffichageTexte(4*largeur/5, hauteur/20, posX + (10 + hauteur/20 )*2, posY+10, police, "C'est perdu !");
  affichageGagne = new AffichageTexte(4*largeur/5, hauteur/20, posX + (10 + hauteur/20 )*2, posY+10, police, "C'est gagné !");
  //affichageFruit = NULL;
  affichageFruit = new AffichageImage(hauteur/10, hauteur/10, posX + (10 + hauteur/20 )*2, (posY+largeur)/2, AffichageObjet::getTexture(partieStandard->getTypeFruit()));
  
  
  if(partieStandard->getEtat()==pause){
    boutons.push_back(pair<TypeBouton, AffichageBouton*> (boutonPause, new AffichageBouton(4*largeur/5, hauteur/10, posX + (10 + hauteur/20 )*4, posY+10, police, "Pause", enfonce)));
  }
  else 
    boutons.push_back(pair<TypeBouton, AffichageBouton*> (boutonPause, new AffichageBouton(4*largeur/5, hauteur/10, posX + (10 + hauteur/20 )*4, posY+10, police, "Pause", normal)));

  boutons.push_back(pair<TypeBouton, AffichageBouton*> (boutonAccueil, new AffichageBouton(4*largeur/5, hauteur/10, posX + (30 + hauteur/20 )*4, posY+10, police, "Retourner Accueil", normal)));  
  
  
  
}

void AffichageMenuJeu::afficher(SDL_Renderer * renderer){
  update();
  affichageScore->afficher(renderer);
  affichageViePacman->afficher(renderer);
  affichageFruit->afficher(renderer);
  
  for(int i = 0; i <boutons.size();i++)
    boutons[i].second->afficher(renderer);

  if(partieStandard->getEtat()==perdu)
    affichagePerdu->afficher(renderer);
  if(partieStandard->getEtat()==gagne)
    affichageGagne->afficher(renderer);
}

void AffichageMenuJeu::chargerPolice(){
  police = TTF_OpenFont("Ressources/Fonts/alterebro-pixel-font.ttf", 30);
  if(!police)
    printf("TTF_OpenFront: %s\n", TTF_GetError()); 
}

void AffichageMenuJeu::update(){
  string score = "Score : ";
  string vie = "Vie : ";
  
  affichageScore->setTexte(score.append(intToString(partieStandard->getPlateau()->getScore())));
  affichageViePacman->setViePacman(partieStandard->getPlateau()->getPacman()->getVie());

}

vector<pair<TypeBouton, AffichageBouton *> > AffichageMenuJeu::getBoutons(){
  return boutons;
}

AffichageMenuJeu::~AffichageMenuJeu(){
  delete affichageViePacman;
  delete affichagePerdu;
  delete affichageGagne;
  delete affichageScore;
  delete affichageFruit;
  TTF_CloseFont(police); // Ne marche pas, laisse donc 820 bytes in 2 blocks 
  for(int i = 0; i <boutons.size();i++)
    delete boutons[i].second;
  boutons.clear();

}

string AffichageMenuJeu::intToString(int i){
  stringstream out;
  out<<i;
  return out.str();
}
