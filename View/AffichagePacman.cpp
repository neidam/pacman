#include "AffichagePacman.hpp"



using namespace std;

AffichageClasse::mapTexture	AffichagePacman::textures = init_map();

AffichagePacman::AffichagePacman(){}

AffichagePacman::AffichagePacman(int posX, int  posY,int largeur, int hauteur, Entite* pacman)
  : AffichageClasse(largeur, hauteur)
{	
	this->pacman = pacman;
	framerateImage = 0;
	this->posX = posX;
	this->posY = posY;
}

AffichagePacman::~AffichagePacman(){
  
}

void AffichagePacman::ajouterTexture(string src, int type, SDL_Renderer* renderer){
	SDL_Texture* newTexture = chargerTexture(src, renderer);
	textures[type] = newTexture;
}

void AffichagePacman::afficher(SDL_Renderer* renderer){
	double angle;
	
	switch (pacman->getDirectionActuelle()){
		case droite:
			angle = 0;
			break;
		case gauche:
			angle = 180;
			break;
		case bas:
			angle = 90;
			break;
		case haut:
			angle = 270;
			break;
	}
	// Inversion des POS obligatoire
	  rect->x = posY + pacman->getPosX();
	  rect->y = posX + pacman->getPosY();

	if (framerateImage < 12)
		SDL_RenderCopyEx(renderer, textures[pacmanOuvert], NULL, rect, angle, NULL, SDL_FLIP_NONE);
	else 
		SDL_RenderCopyEx(renderer, textures[pacmanFerme], NULL, rect, angle, NULL, SDL_FLIP_NONE);
	
	framerateImage++;
	if (framerateImage == 24)
		framerateImage = 0;

}

void AffichagePacman::chargerToutesTextures(SDL_Renderer * renderer){	
  AffichagePacman aff;
  aff.ajouterTexture("Ressources/Images/pacmanOuvert.png", pacmanOuvert, renderer);
  aff.ajouterTexture("Ressources/Images/pacmanFerme.png", pacmanFerme, renderer);
}


void AffichagePacman::supprimerToutesTextures(){
  textures.clear();
}


SDL_Texture * AffichagePacman::getTexture(int type){
  return textures[type];
}
