#include <iostream>
#include "AffichageViePacman.hpp"

using namespace std;

AffichageViePacman::AffichageViePacman(int largeur, int hauteur, int posX, int posY, PartieStandard* partieStandard)
   : ZoneEcran(largeur, hauteur, posX, posY)
{
  viePacman = partieStandard->getPlateau()->getPacman()->getVie();
  int taille = 0;
  if (largeur > hauteur) 
    taille = hauteur;
  else 
    taille = largeur;
  for(int i = 0; i<viePacman; i++){
    listeVie.push_back(new AffichageImage(taille, taille, posX , posY + (i+1) * taille, AffichagePacman::getTexture(pacmanOuvert)));
  }
}
void AffichageViePacman::setViePacman(int vie){
  viePacman = vie;
}

void AffichageViePacman::afficher(SDL_Renderer * renderer){
  for(int i = 0; i<viePacman;i++)
    listeVie[i]->afficher(renderer);

}

AffichageViePacman::~AffichageViePacman(){
  for (int i = 0; i < listeVie.size(); i++)
    delete listeVie[i];
}


