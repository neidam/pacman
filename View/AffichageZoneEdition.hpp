#ifndef AFFICHAGE_ZONE_EDITION
#define AFFICHAGE_ZONE_EDITION

#include <list>
#include "ZoneEcran.hpp"
#include "../Model/Plateau.hpp"
#include "AffichageImage.hpp"
#include "AffichagePlateauJeu.hpp"
#include "AffichageBoutonEdition.hpp"




class AffichageZoneEdition : public ZoneEcran{


  Plateau * plateau;
  AffichagePlateauJeu * affichagePlateauJeu;


  std::vector<AffichageBoutonEdition * >  affichageImage_largeur;
  std::vector<AffichageBoutonEdition * >  affichageImage_hauteur;

public :

  AffichageZoneEdition();

  AffichageZoneEdition(int largeur, int hauteur, int posX, int posY, Plateau * plateau );
  

  void afficher(SDL_Renderer* renderer);

  void resetAffichagePlateau();

  std::vector<AffichageBoutonEdition *> getAffichageImageLargeur();
  std::vector<AffichageBoutonEdition *> getAffichageImageHauteur();
  ~AffichageZoneEdition();




};

#endif
