#include "Ecran.hpp"

#include <cstdlib>

using namespace std;

const int FPS = 60;
const int DELAY_TIME = 1000.0f / FPS;

using namespace std;

Ecran::Ecran(int largeur, int hauteur){

  this->hauteur = hauteur;
  this->largeur = largeur;
  this->controller = new Controller();
  InitSDL();	
  loadTextures();
  //zoneEcran = new AffichagePartie(largeur, hauteur, 0, 0);
  zoneEcran = new AffichageAccueil(largeur, hauteur, 0, 0);
  etat = jouer;
}


void Ecran::afficher(){
  
  Uint32 frameStart, frameTime;
	
  while(controller->getEtat()!=fermer){

    
    testEtatProgramme();
        
    frameStart = SDL_GetTicks();
    
    
    // Affichage du modèle;
    SDL_RenderClear(renderer);	
    
    zoneEcran->afficher(renderer);
    
    		
    SDL_RenderPresent(renderer);
		
     frameTime = SDL_GetTicks() - frameStart;
		
    if (frameTime < DELAY_TIME)
      SDL_Delay((int)(DELAY_TIME - frameTime));
    
  }
  
  //SDL_RenderPresent(renderer);
  
}

void Ecran::testEtatProgramme(){
  if(etat == accueil && controller->getEtat()!=controlAccueil){
    if(controller->getEtat() == controlPartie){
      etat = jouer;
      delete zoneEcran;
      zoneEcran = new AffichagePartie(largeur, hauteur, 0, 0);
    }
    else
      if(controller->getEtat() == controlEditeur){
	etat = editer;
	delete zoneEcran;
	zoneEcran = new AffichageEditeur(largeur, hauteur, 0,0);
      }
  }
  else
    if((etat == jouer && controller->getEtat()!=controlPartie)
       ||
       (etat == editer && controller->getEtat()!=controlEditeur)){
      if(controller->getEtat() == controlAccueil){
	etat = accueil;
	delete zoneEcran;
	zoneEcran = new AffichageAccueil(largeur, hauteur, 0, 0);
      }
    }

  
}

void Ecran::InitSDL(){
  //Initialize SDL
  
  if (SDL_Init(SDL_INIT_VIDEO) < 0){
    printf("SDL cold not initialize! SDL Error: %s\n", SDL_GetError());
  }
  
  else{
    // Set texture filtering to linear
    if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")){
      printf("Warning: Linear texturre filtering not enabled!");
    }
    //Create window
    this->fenetre = SDL_CreateWindow("Pacman", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, largeur, hauteur, 0);
		
    if (fenetre == NULL){
      printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
    }
  
    else{
      //Create renderer for window
      this->renderer = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
      if (renderer == NULL){
	printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
      }
      else{
	//Initialize renderer color
	SDL_SetRenderDrawColor(renderer, 0x0, 0x0, 0x0, 0x0);
				
	//Initialize PNG loading
	int imgFlags = IMG_INIT_PNG;
	if (!(IMG_Init(imgFlags) & imgFlags)){
	  printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
	}
	if(TTF_Init()!=0)
	  printf("TTF_Init : %s\n", TTF_GetError());
      }
    }
  }
}



void Ecran::loadTextures(){
  AffichageCase::chargerToutesTextures(renderer);
  AffichageObjet::chargerToutesTextures(renderer);
  AffichagePacman::chargerToutesTextures(renderer);
  AffichageFantome::chargerToutesTextures(renderer);
  AffichageBouton::chargerToutesTextures(renderer);
  AffichageBoutonEdition::chargerToutesTextures(renderer);
}

void Ecran::deleteTextures(){
  AffichageCase affCase;
  affCase.supprimerToutesTextures();

  AffichageObjet affObjet;
  affObjet.supprimerToutesTextures();
  
  AffichagePacman affPacman;
  affPacman.supprimerToutesTextures();

  
  AffichageFantome affFantome;
  affFantome.supprimerToutesTextures();
  
  AffichageBouton affBouton;
  affBouton.supprimerToutesTextures();
  
}


Ecran::~Ecran(){
  delete zoneEcran;
  deleteTextures();
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(fenetre);
  SDL_Quit();  
  TTF_Quit();
}



