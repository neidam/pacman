#ifndef AFFICHAGE_BOUTON
#define AFFICHAGE_BOUTON

#include "AffichageClasse.hpp"
#include "AffichageImage.hpp"
#include "AffichageTexte.hpp"

enum EtatBouton{normal, enfonce, grise, survol};
enum TypeBouton{boutonPause, boutonJouer, boutonAccueil, boutonEditeur, boutonModeMur, boutonModePassage, boutonModePortail, boutonModePacman, boutonModeFantome, boutonSauvegarder};

class AffichageBouton : public AffichageClasse{

  static mapTexture textures;
  AffichageImage * affichageImage;
  AffichageTexte * affichageTexte;
  EtatBouton etatBouton;
  TTF_Font * police;

public : 
  AffichageBouton();
  AffichageBouton(int largeur, int hauteur, int posX, int posY, TTF_Font * police, std::string texte, EtatBouton etatBouton);

  void afficher(SDL_Renderer* renderer);
  void ajouterTexture(std::string src, int type, SDL_Renderer* renderer);
  static void chargerToutesTextures(SDL_Renderer * renderer);
  void supprimerToutesTextures();
  
  
  EtatBouton getEtat();
  void setEtat(EtatBouton etat);
  bool inBouton(int x, int y);
  
  ~AffichageBouton();
};

#endif
