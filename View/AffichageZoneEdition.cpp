#include <iostream>
#include "AffichageZoneEdition.hpp"

using namespace std;

AffichageZoneEdition::AffichageZoneEdition()
  : ZoneEcran(){ 
}


AffichageZoneEdition::AffichageZoneEdition(int largeur, int hauteur, int posX, int posY, Plateau * plateau )
  : ZoneEcran(largeur, hauteur, posX, posY){
  this->plateau = plateau;
  
  int taille_case = plateau->getTailleCase();
  affichagePlateauJeu = new AffichagePlateauJeu(largeur-taille_case*3, hauteur-taille_case*3, posX+taille_case*2, posY+taille_case*2, plateau);

  for(int i = 1; i<plateau->getHauteur()+3;i++)
    if(i==1 || i==plateau->getHauteur()+2)
      affichageImage_hauteur.push_back(new AffichageBoutonEdition(taille_case, taille_case, posX+i*taille_case, posY, imagePlus));
    else
      affichageImage_hauteur.push_back(new AffichageBoutonEdition(taille_case, taille_case, posX+i*taille_case, posY, imageMoins));

  for(int i = 1; i<plateau->getLargeur()+3;i++)
    if(i==1 || i==plateau->getLargeur()+2)
      affichageImage_largeur.push_back(new AffichageBoutonEdition(taille_case, taille_case, posX, posY+i*taille_case, imagePlus));
    else
      affichageImage_largeur.push_back(new AffichageBoutonEdition(taille_case, taille_case, posX, posY+i*taille_case, imageMoins));
}

void AffichageZoneEdition::afficher(SDL_Renderer * renderer){
  
  if(affichagePlateauJeu) affichagePlateauJeu->afficher(renderer);
  for(int i = 0; i < affichageImage_hauteur.size(); i++)
    affichageImage_hauteur[i]->afficher(renderer);
  for(int i = 0; i < affichageImage_largeur.size(); i++)
    affichageImage_largeur[i]->afficher(renderer);
  
}

vector<AffichageBoutonEdition *> AffichageZoneEdition::getAffichageImageLargeur(){
  return affichageImage_largeur;
}

vector<AffichageBoutonEdition *> AffichageZoneEdition::getAffichageImageHauteur(){
  return affichageImage_hauteur;
}
/* Ce reset est très très sale. Il recharge complètement la classe et il y a moyen d'optimiser */
void AffichageZoneEdition::resetAffichagePlateau(){
  
  //affichagePlateauJeu->resetAffichagePlateau();
  for(int i = 0 ; i < affichageImage_hauteur.size(); i++)
    delete affichageImage_hauteur[i];
  for(int i = 0 ; i < affichageImage_largeur.size(); i++)
    delete affichageImage_largeur[i];
  
  affichageImage_hauteur.clear();
  affichageImage_largeur.clear();
  delete affichagePlateauJeu;
  
  int taille_case = plateau->getTailleCase();
  
   affichagePlateauJeu = new AffichagePlateauJeu(largeur-taille_case*3, hauteur-taille_case*3, posX+taille_case*2, posY+taille_case*2, plateau);

  for(int i = 1; i<plateau->getHauteur()+3;i++)
    if(i==1 || i==plateau->getHauteur()+2)
      affichageImage_hauteur.push_back(new AffichageBoutonEdition(taille_case, taille_case, posX+i*taille_case, posY, imagePlus));
    else
      affichageImage_hauteur.push_back(new AffichageBoutonEdition(taille_case, taille_case, posX+i*taille_case, posY, imageMoins));

  for(int i = 1; i<plateau->getLargeur()+3;i++)
    if(i==1 || i==plateau->getLargeur()+2)
      affichageImage_largeur.push_back(new AffichageBoutonEdition(taille_case, taille_case, posX, posY+i*taille_case, imagePlus));
    else
      affichageImage_largeur.push_back(new AffichageBoutonEdition(taille_case, taille_case, posX, posY+i*taille_case, imageMoins));
  
}


 
AffichageZoneEdition::~AffichageZoneEdition(){
  delete affichagePlateauJeu;
}


      

