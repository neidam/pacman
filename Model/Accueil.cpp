#include "Accueil.hpp"

using namespace std;

Accueil::Accueil()

: EtatProgramme()
{
  plateau = new Plateau("Ressources/Levels/levelAccueil");
  etat = accueil;
}


Accueil::~Accueil(){
  delete plateau;
}

EtatAccueil Accueil::getEtat(){
  return etat;
}
void Accueil::setEtat(EtatAccueil etat){
  this->etat = etat;
}


void Accueil::update()
{
  plateau->getPacman()->deplacer();
  for (int i = 0; i < plateau->getFantomes().size(); i++){	
    plateau->getFantomes()[i]->deplacer();
  }
  
  plateau->verifierCollisionsFantomes();
				    
  
  
  
  
  
  
}


