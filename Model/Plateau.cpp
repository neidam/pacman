#include "Plateau.hpp"
#include "../View/Ecran.hpp"

#include <iostream>
#include <fstream>
#include <stdlib.h>


using namespace std;

Plateau::Plateau(int hauteur, int largeur){
 
  this->hauteur = hauteur;
  this->largeur = largeur;
  caseSortieMaison = NULL;
  calculTailleCase();
  
  for(int i = 0; i<hauteur; i++){
    tab_cases.push_back(deque<Case*>());
    for(int j = 0;j<largeur;j++){
      if(i!=0 && j!=0 && i!=hauteur-1 && j!=largeur-1 && (i%2!=0 || j%2!=0))
	tab_cases[i].push_back(new Passage(tailleCase, i, j));
      else 
	tab_cases[i].push_back(new Mur(tailleCase, i, j));
    }
  }
  associerCases();

  pacman = NULL;
}


  
Plateau::Plateau(string fichier){
	
  chargerFichierPlateau(fichier);
  caseFruit = NULL;
  
}

int Plateau::getTailleCase(){
  return tailleCase;
}

void Plateau::calculTailleCase(){
  int divH = H_ECRAN/(hauteur+3);
  int divL = L_JEU / (largeur+3);
  tailleCase = min(divH, divL);
  
}

void Plateau::chargerFichierPlateau(string fichier){
  ifstream flux(fichier.c_str());
	
  if (flux)
    {
      string indication;
		
      flux >> indication >> hauteur >> largeur;
      int divH = H_ECRAN/(hauteur);
      int divL = L_JEU / (largeur);
      tailleCase = min(divH, divL);
		

		char caractere;
		flux.get(caractere);
		for (int i = 0; i < hauteur; i++){
			tab_cases.push_back(deque <Case*>());
			for (int j = 0; j <= largeur; j++){
				flux.get(caractere);
				if (caractere == 'x'){
					tab_cases[i].push_back(new Mur(tailleCase, i, j));
				}					
				else if (caractere == 'o'){
					tab_cases[i].push_back(new Passage(tailleCase, i, j, new Pacgum()));
				}
				else if (caractere == 'b'){
					tab_cases[i].push_back(new Passage(tailleCase, i, j, new Bonus()));
				}
				else if (caractere == 'p'){
					tab_cases[i].push_back(new Portail(tailleCase, i, j));	
				}
				else if (caractere == 'i'){
					caseSortieMaison = new PorteFantomes(tailleCase, i, j);
					tab_cases[i].push_back(caseSortieMaison);
				}
				else if (caractere == 'm'){
					tab_cases[i].push_back(new MaisonFantomes(tailleCase, i, j));
				}
			}
		}
		
      associerCases();

      int positionX, positionY, vitesse, nbVies;
      string direction;
      flux >> indication >> positionX >> positionY;
      pacman = new Pacman(3, 4, tab_cases[positionX][positionY], droite);
		
      flux >> indication >> positionX >> positionY;
      while (indication != "endFile"){
	fantomes.push_back(new Fantome(3, tab_cases[positionX][positionY], caseSortieMaison, indication, bas, pacman));	
	flux >> indication >> positionX >> positionY;
      }
			
		
      flux.close();		
    }
  else
    {
      cout << "Impossible de lire le fichier" << endl;
    }
}

void Plateau::ecrireFichierPlateau(string nomFichier){
  
  ofstream fichier(nomFichier.c_str(), ios::out | ios::trunc);
  if(fichier){
    fichier << "taillePlateau: " << hauteur <<" "<< largeur<<endl;
    for(int i = 0; i<hauteur; i++){
      for(int j = 0; j<largeur;j++){
	switch(getCase(i, j)->getType()){
	case passage:
	  fichier<<'o';
	  break;
	case portail:
	  fichier<<'p';
	  break;
	case porteFantomes:
	  fichier<<'i';
	  break;
	case maisonFantomes:
	  fichier<<'m';
	  break;
	default:
	  fichier<<'x';
	  break;
	};
      }
      fichier<<endl;
    }

    if(pacman)
      fichier<<"pacman "<<pacman->getPositionCase()->getPosTableauX()<<" "<<pacman->getPositionCase()->getPosTableauY()<<endl;
    string nomF;
    for(int i = 0; i<fantomes.size();i++){
      if(fantomes[i]->getNomFantome()==0)
	nomF = "blinky";
      if(fantomes[i]->getNomFantome()==1)
	nomF = "pinky";
      if(fantomes[i]->getNomFantome()==2)
	nomF = "inky";
      if(fantomes[i]->getNomFantome()==3)
	nomF = "clyde";
      fichier<<nomF<<" "<<fantomes[i]->getPositionCase()->getPosTableauX()<<" "<<fantomes[i]->getPositionCase()->getPosTableauY()<<endl;
    }
    fichier<<"endFile"<<endl;
    fichier.close();
  }
  else
    cout<<"Impossible d'ouvrir le fichier !"<<endl;
  
}

void Plateau::PlateauRandom(){
  //srand (time(NULL));
  int a;
  for(int i = 1; i<hauteur-1; i++)
    for(int j = 1;j<largeur-1;j++){
      a = rand()%3;
      if (a==0){
	tab_cases[i][j]=new Mur(24);
      }
    }
}

Plateau::~Plateau(){
  //cout<<"Delete Plateau : "<<endl;
  if(pacman) delete pacman;
  for (int i = 0; i<fantomes.size();i++)
    delete fantomes[i];
  
  for(int i = 0; i<hauteur;i++)
    for(int j = 0; j<largeur; j++)
      delete tab_cases[i][j];
  
}

int Plateau::getHauteur(){
  return hauteur;
}

int Plateau::getLargeur(){
  return largeur;
}

int Plateau::getNbFantomesDansMaison(){
  int nb = 0;
  for (int i = 0; i < fantomes.size(); i++){
    if (fantomes[i]->getEtat() == attente)
      nb++;
  }
  return nb;		
}

Case* Plateau::getCase(int i, int j){
  return tab_cases[i][j];
}
void Plateau::editerCase(int x, int y, Case * newCase){
    
  if(!(tab_cases[x][y]->getType() == newCase->getType())){
    /* Récupération des cases autours de la case à supprimer*/
    Case * gauche = tab_cases[x][y]->getCaseGauche();
    Case * droite = tab_cases[x][y]->getCaseDroite();
    Case * haut = tab_cases[x][y]->getCaseHaut();
    Case * bas = tab_cases[x][y]->getCaseBas();
 
    if(isCaseType(x, y, portail)){
      if(x == 0)
	haut = NULL;
      if(x == hauteur-1)
	bas = NULL;
      if(y == 0)
	gauche = NULL;
      if(y == largeur-1)
	droite = NULL;
    }
  
    delete tab_cases[x][y];
    tab_cases[x][y] = newCase;
  
    /* Associations de la nouvelle case avec ses cases autour*/
 
    if(gauche) gauche->setCaseDroite(newCase);
    if(droite) droite->setCaseGauche(newCase);
    if(haut) haut->setCaseBas(newCase);
    if(bas) bas->setCaseHaut(newCase);

    /* Et inversement*/
    newCase ->setCaseDroite(droite);
    newCase ->setCaseGauche(gauche);
    newCase ->setCaseHaut(haut);
    newCase ->setCaseBas(bas);
  }
  //afficherTerminal();
 
}

Pacman* Plateau::getPacman(){
  return pacman;
}

int Plateau::getScore(){
  return pacman->getPoints();
}

Passage * Plateau::getCaseFruit(){
  return caseFruit;
  
  
}

void Plateau::retirerPacman(){
  delete pacman;
}

Case * Plateau::placerPacman(int posX, int posY){
  pacman = new Pacman(3, 2, tab_cases[posX][posY], droite);
  return pacman->getPositionCase();
}


void Plateau::setScore(int score){
  pacman->setPoints(score);
}

int Plateau::nbPacgum(){
  int res = 0;
  for(int i = 0; i<hauteur; i++)
    for(int j = 0;j<largeur;j++)
      if(isCaseType(i, j, passage))
	if(tab_cases[i][j]->getContenant()!=NULL)
	  if(tab_cases[i][j]->getContenant()->getType() == pacgum)
	    res++;
  return res;
  
}

deque<Fantome*> Plateau::getFantomes(){
  return fantomes;
  
}

void Plateau::associerCases(){
  for(int i = 0; i<hauteur; i++){
    for(int j = 0;j<largeur;j++){
      // Association vers le haut, la 1ère ligne pointera vers NULL
      if(i>0) tab_cases[i][j]->setCaseHaut(tab_cases[i-1][j]);
      else {
	if(tab_cases[i][j]->getType()==portail)
	  tab_cases[i][j]->setCaseHaut(tab_cases[hauteur-1][j]);
	else
	  tab_cases[i][j]->setCaseHaut(NULL);
	    
      }
	  
      // Association vers le bas, la dernière ligne pointera vers NULL
      if(i<hauteur-1) tab_cases[i][j]->setCaseBas(tab_cases[i+1][j]);
      else
	if(tab_cases[i][j]->getType()==portail)
	  tab_cases[i][j]->setCaseBas(tab_cases[0][j]);
	else 
	  tab_cases[i][j]->setCaseBas(NULL);
	  
	  
      // Association vers la gauche, la 1ère ligne pointera vers NULL
      if(j>0) tab_cases[i][j]->setCaseGauche(tab_cases[i][j-1]);
      else
	if(tab_cases[i][j]->getType()==portail)
	  tab_cases[i][j]->setCaseGauche(tab_cases[i][largeur-1]);
	else
	  tab_cases[i][j]->setCaseGauche(NULL);

      // Association vers la droite, la dernière ligne pointera vers NULL
      if(j<largeur-1) tab_cases[i][j]->setCaseDroite(tab_cases[i][j+1]);
      else
	if(tab_cases[i][j]->getType()==portail)
	  tab_cases[i][j]->setCaseDroite(tab_cases[i][0]);
	else
	  tab_cases[i][j]->setCaseDroite(NULL);
	  
    }
  }
}

void Plateau::afficherTerminal(){
  for(int i = 0; i<hauteur; i++){
    for(int j = 0; j<largeur; j++)
      tab_cases[i][j]->afficherTerminal();
    cout<<endl;
  }
}


bool Plateau::verifierCollisionsFantomes(){
	for (int i = 0; i < fantomes.size(); i++){
		if (fantomes[i]->getPositionCase() == pacman->getPositionCase()){
			if (fantomes[i]->getEtat() == poursuite){
				pacman->perdreVie();
				if(pacman->getVie()>0){
				  pacman->reinitialiserPos();
				  for (int i = 0; i < fantomes.size(); i++){
					fantomes[i]->reinitialiserPos();
				  }
				}
				return false;
			}
			else if (fantomes[i]->getEtat() == fuite){
				fantomes[i]->setEtat(retourMaison);
			}
		}
	}
	return true;
}
    

void Plateau::libererFantome(){
	for (int i = 0; i < fantomes.size(); i++){
		if (fantomes[i]->getEtat() == attente){
			fantomes[i]->setEtat(sortMaison);
			fantomes[i]->definirCaseCible();
			fantomes[i]->definirProchaineDirection();
			break;
		}
	}

}


void Plateau::placerFruit(TypeObjet fruit){
  vector <Passage * > casesLibres;
  for(int i = 0; i< tab_cases.size();i++){
    for(int j = 0; j<tab_cases[i].size(); j++)
      if(tab_cases[i][j]->getType()==passage && tab_cases[i][j]->getContenant() == NULL)
	casesLibres.push_back((Passage*) tab_cases[i][j]);
  }
  
  int r = rand() % casesLibres.size();
  
  
  casesLibres[r]->placerFruit(fruit);
  caseFruit = casesLibres[r];
  
}

void Plateau::ajouterLigneHaut(){
  hauteur++;
  calculTailleCase();
  
  deque<Case * > newLigne;
  for(int i = 0; i<largeur;i++){
    newLigne.push_back(new Mur(tailleCase, i, 0));
  }
  tab_cases.push_front(newLigne);
  

  for(int i = 0; i<largeur;i++){
    if(i==0)
      tab_cases[0][i]->setCaseGauche(NULL);
    else
      tab_cases[0][i]->setCaseGauche(tab_cases[0][i-1]);
    tab_cases[0][i]->setCaseBas(tab_cases[1][i]);
    tab_cases[1][i]->setCaseHaut(tab_cases[0][i]);
    if(i==largeur-1)
      tab_cases[0][i]->setCaseDroite(NULL);
    else
      tab_cases[0][i]->setCaseDroite(tab_cases[0][i+1]);
  }
  for(int i = 0 ; i<hauteur; i++)
    for(int j = 0 ; j<largeur; j++)
      tab_cases[i][j]->mettreAJour(tailleCase, i, j);
  
}

void Plateau::ajouterLigneBas(){

  hauteur++;
  calculTailleCase();
  
  
  deque<Case * > newLigne;
  for(int i = 0; i<largeur;i++){
    newLigne.push_back(new Mur(tailleCase, i, 0));
  }
  tab_cases.push_back(newLigne);
  

  for(int i = 0; i<largeur;i++){
    if(i==0)
      tab_cases[hauteur-1][i]->setCaseGauche(NULL);
    else
      tab_cases[hauteur-1][i]->setCaseGauche(tab_cases[hauteur-1][i-1]);
    tab_cases[hauteur-1][i]->setCaseHaut(tab_cases[hauteur-2][i]);
    tab_cases[hauteur-2][i]->setCaseBas(tab_cases[hauteur-1][i]);
    if(i==largeur-1)
      tab_cases[hauteur-1][i]->setCaseDroite(NULL);
    else
      tab_cases[hauteur-1][i]->setCaseDroite(tab_cases[hauteur-1][i+1]);
  }
  for(int i = 0 ; i<hauteur; i++)
    for(int j = 0 ; j<largeur; j++)
      tab_cases[i][j]->mettreAJour(tailleCase, i, j);
}
void Plateau::ajouterColonneGauche(){
 
  largeur++;
  calculTailleCase();
  
  deque<Case * > newLigne;
  for(int i = 0; i<hauteur;i++)
    tab_cases[i].push_front(new Mur(tailleCase, 0, i));
  
			    
  for(int i = 0; i<hauteur;i++){
    if(i==0)
      tab_cases[i][0]->setCaseHaut(NULL);
    else
      tab_cases[i][0]->setCaseHaut(tab_cases[i-1][0]);
    tab_cases[i][0]->setCaseDroite(tab_cases[i][1]);
    tab_cases[i][1]->setCaseGauche(tab_cases[i][0]);
    if(i==hauteur-1)
      tab_cases[i][0]->setCaseBas(NULL);
    else
      tab_cases[i][0]->setCaseBas(tab_cases[i+1][0]);
  }
  for(int i = 0 ; i<hauteur; i++)
    for(int j = 0 ; j<largeur; j++)
      tab_cases[i][j]->mettreAJour(tailleCase, i, j);
}
void Plateau::ajouterColonneDroite(){
  largeur++;
  calculTailleCase();
  
  for(int i = 0; i<hauteur;i++)
    tab_cases[i].push_back(new Mur(tailleCase, 0, i));
  
			    
  for(int i = 0; i<hauteur;i++){
    if(i==0)
      tab_cases[i][largeur-1]->setCaseHaut(NULL);
    else
      tab_cases[i][largeur-1]->setCaseHaut(tab_cases[i-1][largeur-1]);
    tab_cases[i][largeur-1]->setCaseGauche(tab_cases[i][largeur-2]);
    tab_cases[i][largeur-2]->setCaseDroite(tab_cases[i][largeur-1]);
    if(i==hauteur-1)
      tab_cases[i][largeur-1]->setCaseBas(NULL);
    else
      tab_cases[i][largeur-1]->setCaseBas(tab_cases[i+1][largeur-1]);
  }

  for(int i = 0 ; i<hauteur; i++)
    for(int j = 0 ; j<largeur; j++)
      tab_cases[i][j]->mettreAJour(tailleCase,i,j);
}


void Plateau::supprimerLigne(int num){
  hauteur--;
  calculTailleCase();
  deque<Case *> deleteLine = tab_cases[num];
  tab_cases.erase(tab_cases.begin()+num);

  for(int i =0; i<deleteLine.size();i++){

    if(isInMaisonFantomes(deleteLine[i]->getPosTableauX(),deleteLine[i]->getPosTableauY())){
      supprimerMaisonFantomes();
      
    }
    if(deleteLine[i]->getCaseHaut())
      deleteLine[i]->getCaseHaut()->setCaseBas( deleteLine[i]->getCaseBas());
    if(deleteLine[i]->getCaseBas())
      deleteLine[i]->getCaseBas()->setCaseHaut( deleteLine[i]->getCaseHaut());
    if(pacman && pacman->getPositionCase()==deleteLine[i]){
      delete pacman;
      pacman = NULL;
    }
    delete deleteLine[i];
  }
  
  for(int i = 0 ; i<hauteur; i++)
    for(int j = 0 ; j<largeur; j++)
      tab_cases[i][j]->mettreAJour(tailleCase,i,j);

}
void Plateau::supprimerColonne(int num){
  largeur--;
  calculTailleCase();
  
  deque<Case *> deleteColumn;
  for(int i =0;i<tab_cases.size();i++){
    deleteColumn.push_back((tab_cases[i][num]));
    tab_cases[i].erase(tab_cases[i].begin()+num);
  }
  
  for(int i =0; i<deleteColumn.size();i++){
    if(isInMaisonFantomes(deleteColumn[i]->getPosTableauX(),deleteColumn[i]->getPosTableauY()))
      supprimerMaisonFantomes();
    if(deleteColumn[i]->getCaseGauche())
      deleteColumn[i]->getCaseGauche()->setCaseDroite( deleteColumn[i]->getCaseDroite());
    if(deleteColumn[i]->getCaseDroite())
      deleteColumn[i]->getCaseDroite()->setCaseGauche( deleteColumn[i]->getCaseGauche());
    if(pacman && pacman->getPositionCase()==deleteColumn[i]){
      delete pacman;
      pacman = NULL;
    }
    delete deleteColumn[i];
  }

  for(int i = 0 ; i<hauteur; i++)
    for(int j = 0 ; j<largeur; j++)
      tab_cases[i][j]->mettreAJour(tailleCase,i,j);

}

bool Plateau::estValide(){
  // Plateau entouré par des murs ou si Portail, verifier qu'un autre Portail  soit à l'opposé
  // Aucun cul de sac
  // Aucun paté de 4 passage
  // Aucun passage en dehors des bords
  // Touts les passages sont accessible entre eux.

  bool res = true;
  int  cpt = 0;
  for(int i = 0; i<hauteur;i++)
    for(int j = 0; j<largeur;j++){
      if(tab_cases[i][j]->getType()==passage)
	cpt++;
      if(!validationRebord(i, j)){
	cout<<"Les rebords ne sont pas valide"<<endl;
	res = false;
      }
      if(!validationPassage(i, j)){
	cout<<"Les passages ne sont pas valide"<<endl;
	res = false;
      }
      if(!validationMaisonFantomes(i, j)){
	cout<<"La maison Fantomes n'est pas valide"<<endl;
	res = false;
      }
    }
  
  if(!pacman){
    cout<<"Il manque le Pacman !"<<endl;
    res = false;
  }
  else 
    for(int i = 0; i<fantomes.size();i++)
      if(pacman->getPositionCase()->getPosTableauX()==fantomes[i]->getPositionCase()->getPosTableauX() && 
	 pacman->getPositionCase()->getPosTableauY()==fantomes[i]->getPositionCase()->getPosTableauY()) {
	cout<<"Le Pacman ne peut pas être sur la même case qu'un Fantome"<<endl;
	res = false;
      }
  
  
  if(cpt<(hauteur*largeur)/3){
    cout<<"Pas assez de passage dans ce plateau !"<<endl;
    res = false;
  }
  if(res)
    res = validationUniqueChemin();
  return res;
  
}

bool Plateau::validationRebord(int i, int j){
  bool res = true;
  if(i == 0 || i == hauteur-1)
    if(!tab_cases[i][j]->isMur()) 
      if((tab_cases[i][j]->getType()==portail)){
	if(i==0 & !(tab_cases[hauteur-1][j]->getType()==portail)
	   || tab_cases[i][j+1]->getType()==portail ){
	  res = false;
	}
	if(i==hauteur-1 & !(tab_cases[0][j]->getType()==portail))
	  res = false;
	
      }
      else
	res = false; 
  
  if(j==0 || j == largeur-1)
    if(!tab_cases[i][j]->isMur())
      if((tab_cases[i][j]->getType()==portail)){
	if(j==0 & !(tab_cases[i][largeur-1]->getType()==portail)
	   || tab_cases[i+1][j]->getType()==portail)
	  res = false;
	if(j==largeur-1 & !(tab_cases[i][0]->getType()==portail))
	  res = false;
      }
      else
	res = false; 
  
  return res;
}
/*
 * Vérifie la présence de cul de Sac, les paté de 4 passage et la présence de portail dans le plateau
 */
 
bool Plateau::validationPassage(int i, int j){
  bool res;
  if(i>0 & i<hauteur-1 & j>0 & j<largeur-1)
    if(tab_cases[i][j]->getType()==passage){
      tab_cases[i][j]->getTabAdjMurs();
      if(tab_cases[i][j]->nbMurAdj()>2){
	cout<<"Cul de sac en "<<i<<", "<<j<<endl;
	res = false;
      }
      if(!(((Passage*)(tab_cases[i][j]))->isValide())){
	cout<<"Paté en "<<i<<", "<<j<<endl;
	res = false;
      }
    }
    else
      if(tab_cases[i][j]->getType()==portail){
	cout<<"Portail en "<<i<<", "<<j<<endl;
	res = false;
      }
  
  return res;
}

bool Plateau::isCaseType(int x, int y, TypeCase type){
  return tab_cases[x][y]->getType()==type;
}
 
bool Plateau::validationMaisonFantomes(int x, int y){
  if(tab_cases[x][y]->getType()==porteFantomes){
    if(x < 2 || x+2 >= hauteur || y-2 < 0 || y+2 >= largeur){
      cout<<"Maison Fantome imcomplète"<<endl;
      return false;
    }
    if(tab_cases[x-1][y]->getType()!=passage){
      cout<<"La case au dessus de la porte doit être un passage !"<<endl;
      return false;
    }
    for(int i = x; i<=x+2;i++){
      for(int j = y-2; j<=y+2;j++){
	/* Test Mur */
	if(i==x && j!=y)
	  if(!tab_cases[i][j]->isMur()){
	    cout<<"Première ligne, pas de mur en "<<i<<", "<<j<<endl;
	    return false;
	  }
	if(i==x+1)
	  if (j==y-2 || j == y+2){
	    if(!tab_cases[i][j]->isMur()){
	      cout<<"Deuxième ligne, pas de mur en "<<i<<", "<<j<<endl;
	      return false; 
	    }
	  }
	  else if(!isCaseType(i, j, maisonFantomes)){
	    cout<<"Deuxième ligne, pas de maisonFantome en "<<i<<", "<<j<<endl;
	    return false; 
	  }
	
	if(i==x+2)
	  if(!tab_cases[i][j]->isMur()){
	    cout<<"Troisième ligne, pas de mur en "<<i<<", "<<j<<endl;
	    return false; 
	  }

      }
    }
  }
  return true;
}

bool Plateau::validationUniqueChemin(){
  for(int i = 1; i<hauteur-1; i++)
    for(int j = 1; j<largeur-1;j++){
      if(tab_cases[i][j]->getType()==passage){
	((Passage*)tab_cases[i][j])->verifChemin();
	 
	i+=hauteur; // Sors de la boucle
	j+=largeur;
      }
       
    }
  bool res = true;
  for(int i = 1; i<hauteur-1; i++)
    for(int j = 1; j<largeur-1;j++){
      if(tab_cases[i][j]->getType()==passage){
	if(!((Passage*)tab_cases[i][j])->getVerif())
	  res = false;
	((Passage*)tab_cases[i][j])->resetVerif();
      }
    }
  if(!res)
    cout<<"Il existe des passages isolés !"<<endl;
  return res; 
}

void Plateau::supprimerMaisonFantomes(){
  if(caseSortieMaison){
      int auxH = caseSortieMaison->getPosTableauX()+2;
      int auxL = caseSortieMaison->getPosTableauY()+2;
      for(int i = caseSortieMaison->getPosTableauX(); i <= auxH; i++)
	for(int j = caseSortieMaison->getPosTableauY()-2; j <= auxL;j++){
	  editerCase(i, j, new Mur(tailleCase, i, j));
	}
      for(int i = 0; i<fantomes.size();i++)
	delete fantomes[i];
      fantomes.clear();
    }
    caseSortieMaison = NULL;

}

void Plateau::placerMaisonFantomes(int x, int y){
  if((y+2<largeur) && (y-2 >=0) && (x+2 < hauteur) && x>0){
    supprimerMaisonFantomes();
  
    // 1ere ligne 
    editerCase(x, y-2, new Mur(tailleCase, x, y-2));
    editerCase(x, y-1, new Mur(tailleCase, x, y-1));
    caseSortieMaison = new PorteFantomes(tailleCase, x, y);
    editerCase(x, y, caseSortieMaison);
    editerCase(x, y+1, new Mur(tailleCase, x, y+1));
    editerCase(x, y+2, new Mur(tailleCase, x, y+2));
    
    // 2e ligne
    editerCase(x+1, y-2, new Mur(tailleCase, x+1, y-2));
    editerCase(x+1, y-1, new MaisonFantomes(tailleCase, x+1, y-1));
    editerCase(x+1, y, new MaisonFantomes(tailleCase, x+1, y));
    editerCase(x+1, y+1, new MaisonFantomes(tailleCase, x+1, y+1));
    placerFantomes(x, y);
    editerCase(x+1, y+2, new Mur(tailleCase, x+1, y+2));

    //3e Ligne
    editerCase(x+2, y-2, new Mur(tailleCase, x+2, y-2));
    editerCase(x+2, y-1, new Mur(tailleCase, x+2, y-1));
    editerCase(x+2, y, new Mur(tailleCase, x+2, y));
    editerCase(x+2, y+1, new Mur(tailleCase, x+2, y+1));
    editerCase(x+2, y+2, new Mur(tailleCase, x+2, y+2));
  }
  

}

bool Plateau::isInMaisonFantomes(int x, int y){
  if(caseSortieMaison){
 
    bool res = false;
    if(caseSortieMaison->getPosTableauX()-1==x && caseSortieMaison->getPosTableauY()==y)
      res = true;
    if(caseSortieMaison->getPosTableauX()==x && y >=caseSortieMaison->getPosTableauY()-2  && y<=caseSortieMaison->getPosTableauY()+2 ){
      res = true;
    }
    if(caseSortieMaison->getPosTableauX()+1==x && caseSortieMaison->getPosTableauY()-2 <= y && caseSortieMaison->getPosTableauY()+2 >= y)
      res = true;
    if(caseSortieMaison->getPosTableauX()+2==x && caseSortieMaison->getPosTableauY()-2 <= y && caseSortieMaison->getPosTableauY()+2 >= y)
      res = true;
  return res;
  }
  else
    return false;
}

void Plateau::placerFantomes(int x, int y){

  if(tab_cases[x-1][y]->getType()!=passage)
    editerCase(x-1, y, new Passage(tailleCase, x-1, y));
  fantomes.push_back(new Fantome(2, tab_cases[x-1][y], "blinky",bas));
  fantomes.push_back(new Fantome(2, tab_cases[x+1][y], "inky",bas)); 
  fantomes.push_back(new Fantome(2, tab_cases[x+1][y-1], "pinky",bas)); 
  fantomes.push_back(new Fantome(2, tab_cases[x+1][y+1], "clyde",bas)); 
}

