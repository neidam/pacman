#ifndef ETAT_PROGRAMME
#define ETAT_PROGRAMME

#include "Plateau.hpp"



class EtatProgramme{

protected:
   Plateau* plateau;

public:
  
  EtatProgramme();
  Plateau* getPlateau();

  virtual void update()=0;
};

#endif
