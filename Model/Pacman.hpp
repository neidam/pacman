#ifndef PACMAN
#define PACMAN

#include "Entite.hpp"

enum EtatPacman{
	fuit,
	invincible
};


class Pacman : public Entite {

  int points;
  int vie;
  EtatPacman etat;
  int tmpInvincible;
  int cptInvincible;
  
public:
  

  Pacman(int vie, int vitesse, Case* positionCase,Sens direction);
  
  ~Pacman();
	
  int getPoints();
  EtatPacman getEtat();
  int getVie();
  int getTmpInvincible();
  int getCptInvincible();
  void setPoints(int newPoints);
  void setVie(int newVie);
  void decreaseCptInvincible();
	
  void perdreVie();

  void deplacer();
  void reinitialiserPos();
	
  
};
#endif
