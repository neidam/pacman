#include "Fruit.hpp"


Fruit::Fruit(TypeObjet type){
  typeFruit = type;
}

TypeObjet Fruit::getType(){
  return typeFruit;
}

int Fruit::getPoint(){
  switch(typeFruit){

  case cerise:
    return 100;
    break;
  case fraise:
    return 300;
    break;
  case orange:
    return 500;
    break;
  case pomme:
    return 700;
    break;
  case melon:
    return 1000;
    break;
  case galboss:
    return 2000;
    break;
  case cloche:
    return 3000;
    break;
  case cle:
    return 5000;
    break;
  }
}
