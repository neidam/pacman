#ifndef ENTITE
#define ENTITE

#include "Passage.hpp"

enum Sens{
	gauche,
	droite,
	haut,
	bas,
};


class Entite {
	
	
	
	void changerDirection(Sens nDirection);
	void testerChangementDirection(bool changementVertical);

	protected:
	
		int posX;
		int posY;
		int vitesse;
		
		float avancement;
		Case* caseDepart;
		Case* positionCase;
		Sens directionActuelle;
		Sens directionDepart;
		Sens prochaineDirection;
		
		
	
	public:
	
		Entite(int vitesse, Case* positionCase, Sens direction);
		~Entite();

		bool deplacement();
		virtual void reinitialiserPos()=0;
		virtual void deplacer()=0;
		
		int getPosX();
		int getPosY();	
		Sens getDirectionActuelle();
		Sens getProchaineDirection();
		Case* getPositionCase();
		void setVitesse(int vitesse);
		
		void calculPos();
		void setProchaineDirection(Sens dir);
  
};
#endif
