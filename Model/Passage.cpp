#include "Passage.hpp"

using namespace std;

Passage::Passage(int tailleCase)
  : Case(tailleCase)
{
  objet = new Pacgum();
  verif = false;
}

Passage::Passage(int tailleCase, int posTableauX, int posTableauY)
  : Case(tailleCase, posTableauX, posTableauY)
{
  this->objet = new Pacgum();
  verif = false;
}

Passage::Passage(int tailleCase, int posTableauX, int posTableauY, Bonus* bonus)
  : Case(tailleCase, posTableauX, posTableauY)
{
  this->objet = bonus;
  verif = false;
}

Passage::Passage(int tailleCase, int posTableauX, int posTableauY, Pacgum* pacgum)
  : Case(tailleCase, posTableauX, posTableauY)
{
  this->objet = pacgum;
  verif = false;
}

Passage::~Passage(){
  delete objet;
}

bool Passage::isMur(){
  return false;
}
bool Passage::isValide(){
 
  getTabAdjMurs();
  getTabDiagoMurs();
  int cpt = 0;
  if(getCaseDroite() && getCaseDroite()->getType()==passage)
    cpt++;
  if(getCaseBas() && getCaseBas()->getType()==passage){
    cpt++;
    if(getCaseBas()->getCaseDroite() && getCaseBas()->getCaseDroite()->getType()==passage)
      cpt++;
  }
  return cpt<3;
  
}

TypeCase Passage::getType(){
  return passage;
}

Objet * Passage::getContenant(){
  return objet;
}

bool Passage::getVerif(){
  return verif;
}

void Passage::resetVerif(){
  verif = false;
}

void Passage::afficherTerminal(){
  if(objet == NULL)
    cout<<" ";
  else
    cout<<".";
}

int Passage::mangerGum(){
  int aux = objet->getPoint();
  delete objet;
  objet = NULL;
  return aux;
}

void Passage::placerFruit(TypeObjet fruit){

  objet = new Fruit(fruit);
  //cout<<"Un fruit a été placé !"<<endl;
}

	       
bool Passage::verifChemin(){

  if(verif)
    return verif;
  //cout<<"Verification de "<<posTableauX<<", "<<posTableauY<<endl;
  verif = true;
  bool resGauche = false;
  bool resDroite = false;
  bool resBas = false;
  bool resHaut = false;
  if(caseDroite){
    if(caseDroite->getType()==passage)
      resDroite = ((Passage*) caseDroite)->verifChemin();
    else if (caseDroite->getType()==portail){
      resDroite = true;
      //cout<<(caseDroite->getCaseDroite())<<endl;
      resDroite = ((Passage*)caseDroite->getCaseDroite()->getCaseDroite())->verifChemin();
    }
    if(caseDroite->isMur())
      resDroite = true;
  }
  if(caseGauche){
    if(caseGauche->getType()==passage)
      resGauche = ((Passage*) caseGauche)->verifChemin();
    else if (caseGauche->getType()==portail)
      //resGauche = true;
      resGauche = ((Passage*)caseGauche->getCaseGauche()->getCaseGauche())->verifChemin();
    if(caseGauche->isMur())
      resGauche = true;
  }
  if(caseBas){
    if(caseBas->getType()==passage)
      resBas = ((Passage*) caseBas)->verifChemin();
    else if (caseBas->getType()==portail)
      // resBas = true;
      resBas = ((Passage*)caseBas->getCaseBas()->getCaseBas())->verifChemin();
    if(caseBas->isMur())
      resBas = true;
  }
  if(caseHaut){
    if(caseHaut->getType()==passage)
      resHaut = ((Passage*) caseHaut)->verifChemin();
    else if (caseHaut->getType()==portail)
      resHaut = ((Passage*)caseHaut->getCaseHaut()->getCaseHaut())->verifChemin();
    //resHaut = true;
    if(caseHaut->isMur())
      resHaut = true;
  }
  return resGauche & resDroite & resBas & resHaut;
}
