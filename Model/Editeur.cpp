#include "Editeur.hpp"

using namespace std;

Editeur::Editeur()
: EtatProgramme()
{
  plateau = new Plateau(11, 11);
  //plateau = new Plateau("testEdit");
  posPacman = NULL;
 
}



Editeur::~Editeur(){
  delete plateau;
}

void Editeur::editerCase(int SDL_x, int SDL_y, EtatEditeurCase etat){
  
  /* Inversion obligatoire*/
  int tailleCase = plateau->getTailleCase();
  int posY = (SDL_x - (L_ECRAN - L_JEU+2*tailleCase))/tailleCase;
  int posX = (SDL_y - (H_ECRAN - H_JEU+2*tailleCase))/tailleCase;
  
  
  switch(etat){

  case placerMur:
    plateau->editerCase(posX, posY, new Mur(tailleCase, posX, posY));
    break;
  case placerPassage:
    plateau->editerCase(posX, posY, new Passage(tailleCase, posX, posY));
    break;
  case placerPortail:
    // Cette grosse condition regarde si on essai pas de placer un portail autre part que dans les bords du Plateau. Interdit aussi d'en placer dans les coins
    if(posX>0 && posX<plateau->getHauteur()-1 && posY>0 && posY<plateau->getLargeur()-1 || (posX==0 && posY==0) || (posX==0 && posY==plateau->getLargeur()-1) || (posX == plateau->getHauteur()-1 &&  posY==0) || (posX == plateau->getHauteur() && posY==plateau->getLargeur()-1))
      cout<<"Impossible de placer un Portail ici !"<<endl;
    else{
      Portail * newPortail1 = new Portail(tailleCase, posX, posY);
      plateau->editerCase(posX, posY, newPortail1);  
      if(posX==0){
	Portail * newPortail2 =  new Portail(tailleCase, plateau->getHauteur()-1,posY);
	plateau->editerCase(plateau->getHauteur()-1,posY, newPortail2);
	newPortail1->setCaseHaut(newPortail2);
	newPortail2->setCaseBas(newPortail1);
	
      }
      if(posX == plateau->getHauteur()-1){
	Portail * newPortail2 = new Portail(tailleCase, 0, posY);
	plateau->editerCase(0,posY, newPortail2); 
	newPortail1->setCaseBas(newPortail2);
	newPortail2->setCaseHaut(newPortail1);
      }
      if(posY==0){
	Portail * newPortail2 = new Portail(tailleCase, posX, plateau->getLargeur()-1);
	plateau->editerCase(posX,plateau->getLargeur()-1, newPortail2);
	newPortail1->setCaseGauche(newPortail2);
	newPortail2->setCaseDroite(newPortail1);
      }
      if(posY == plateau->getLargeur()-1){
	Portail * newPortail2 = new Portail(tailleCase, posX, 0);
	plateau->editerCase(posX ,0, newPortail2); 
	newPortail1->setCaseDroite(newPortail2);
	newPortail2->setCaseGauche(newPortail1);
      }
      
    }
    break;
  case placerPacman:
    if(posPacman)
      plateau->retirerPacman();
    plateau->editerCase(posX, posY, new Passage(tailleCase, posX, posY));
    posPacman = plateau->placerPacman(posX, posY);
    break;
  case placerMaisonFantome:
    plateau->placerMaisonFantomes(posX, posY);
    break;
  }
}

int Editeur::getTailleCasePlateau(){
  return plateau->getTailleCase();
}

void Editeur::update(){
  
}

void Editeur::ajouterLigneOuColonne(Sens s){
  switch(s){
  case haut:
    plateau->ajouterLigneHaut();
    break;
  case bas:
    plateau->ajouterLigneBas();
    break;
  case gauche:
    plateau->ajouterColonneGauche();
    break;
  case droite:
    plateau->ajouterColonneDroite();
    break;
  }
}
void Editeur::supprimerLigne(int num){
  //cout<<"Supression de la ligne "<<num<<endl;
  if(plateau->getHauteur()>1)
    plateau->supprimerLigne(num);
  else
    cout<<"Impossible de supprimer la dernière ligne !"<<endl;
}
void Editeur::supprimerColonne(int num){
  

  if(plateau->getLargeur()>1)
    plateau->supprimerColonne(num);
  else
    cout<<"Impossible de supprimer la dernière colonne !"<<endl;
}

void Editeur::sauvegarderPlateau(){
  if(plateau->estValide()){
    plateau->ecrireFichierPlateau("Toto.txt");
    cout<<"Sauvegarde effectué !"<<endl;
  }
  else
    cout<<"Ce plateau est invalide !"<<endl;
  //plateau->afficherTerminal();
}
