#ifndef MAISON_FANTOMES
#define MAISON_FANTOMES

#include "Case.hpp"

class MaisonFantomes : public Case{
	
public: 

  MaisonFantomes(int tailleCase);
  MaisonFantomes(int tailleCase, int posTableauX, int posTableauY);
  ~MaisonFantomes();
  
  bool isMur();
  TypeCase getType();
  Objet * getContenant();
  void afficherTerminal();
  
};

  
#endif
