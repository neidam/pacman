#ifndef PARTIE_STANDARD
#define PARTIE_STANDARD

#include "EtatProgramme.hpp"
#include "Objet.hpp"

enum EtatPartie{pause, en_cours, gagne, perdu, fini};

class PartieStandard : public EtatProgramme{

  EtatPartie etat;
  int niveau;
  int cptFrameFantomes;
  
public:
  PartieStandard(std::string nomFichier);
  ~PartieStandard();

  EtatPartie getEtat();
  int getNiveau();

  TypeObjet getTypeFruit();
  void setEtat(EtatPartie etat);
  void niveauSuivant();
    
  void update();
  
};

#endif
