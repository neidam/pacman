#include "Mur.hpp"

using namespace std;

Mur::Mur(int tailleCase)
	: Case(tailleCase)
{
}

Mur::Mur(int tailleCase, int posTableauX, int posTableauY)
	: Case(tailleCase, posTableauX, posTableauY)
{
}

Mur::~Mur(){
}

bool Mur::isMur(){
  return true;
}

void Mur::afficherTerminal(){
  std::cout<<"x";
}

TypeCase Mur::getType(){

  // cout<<nbMurAdj()<<endl;
  getTabAdjMurs(); // Nécéssaire pour initialiser les tableaux 
  getTabDiagoMurs();
  switch(nbMurAdj()){
  case 0 :
    return getTypeMur0();
    break;
  case 1 :
    return getTypeMur1();
    break;
  case 2 :
    return getTypeMur2();
    break;
  case 3 :
    return getTypeMur3();
    break;
  case 4:
    return getTypeMur4();
  }
}

TypeCase Mur::getTypeMur0(){
  return seul;
}

TypeCase Mur::getTypeMur1(){
  if(isMurHaut())
    angle = 0;
  if(isMurDroite())
    angle = 90;
  if(isMurBas())
    angle = 180;
  if(isMurGauche())
    angle = 270;
  return pic;
}

TypeCase Mur::getTypeMur2(){
  // Les deux murs sont opposé ?
  if(tabAdjMurs[0] && tabAdjMurs[1] ){
    angle = 0;
    return ligne_vide;
  }
  if (tabAdjMurs[2] && tabAdjMurs[3]){
    angle = 90;
    return ligne_vide;
  }
  else {
    if(tabAdjMurs[1] && tabAdjMurs[3])
      angle = 0;
    if(tabAdjMurs[0] && tabAdjMurs[3])
      angle = 90;
    if(tabAdjMurs[0] && tabAdjMurs[2])
      angle = 180;
    if(tabAdjMurs[1] && tabAdjMurs[2])
      angle = 270;
    if(getNbDiago())
      return coin_plein;
    else
      return coin_vide;
  }
}

TypeCase Mur::getTypeMur3(){

  if(!tabAdjMurs[0])
    angle = 0;
  if(!tabAdjMurs[1])
    angle = 180;
  if(!tabAdjMurs[2])
    angle = 90;
  if(!tabAdjMurs[3])
    angle = 270;

  int nbDiago = getNbDiago();
  if (nbDiago==0)
    return double_coin_vide;
  if (nbDiago==1){
    int aux;
    if(!tabAdjMurs[0])
      aux = 1;
    if(!tabAdjMurs[1])
      aux = 3;
    if(!tabAdjMurs[2])
      aux = 2;
    if(!tabAdjMurs[3])
      aux = 0;
    if(tabDiagoMurs[aux])
      return double_coin_imcomplet;
    else 
       return double_coin_imcomplet_bis;
  }
  if(nbDiago==2)
    return ligne_plein;
}


TypeCase Mur::getTypeMur4(){
  int nbDiago = getNbDiago();
  if(nbDiago==4)
    return plein;
  if (nbDiago==3){
    if(!tabDiagoMurs[2])
      angle = 0;
    if(!tabDiagoMurs[3])
      angle = 90;
    if(!tabDiagoMurs[0])
      angle = 180;
    if(!tabDiagoMurs[1])
      angle = 270;
    return coin_complet;
  }
  if(nbDiago==2){
    if((tabDiagoMurs[0] && tabDiagoMurs[2]) || (tabDiagoMurs[3] && tabDiagoMurs[1])){
      if(tabDiagoMurs[1] && tabDiagoMurs[3])
	angle = 90;
      else 
	angle = 0;
      return sablier;
    }
    else {
      if (tabDiagoMurs[1] && tabDiagoMurs[2])
	angle = 180;
      if (tabDiagoMurs[2] && tabDiagoMurs[3])
	angle = 270;
      if (tabDiagoMurs[3] && tabDiagoMurs[0])
	angle = 0;
      if (tabDiagoMurs[0] && tabDiagoMurs[1])
	angle = 90;
      return double_coin_plein;
    }
  }
  if (nbDiago==1){
    if(tabDiagoMurs[2])
      angle = 0;
    if(tabDiagoMurs[3])
      angle = 90;
    if(tabDiagoMurs[0])
      angle = 180;
    if(tabDiagoMurs[1])
      angle = 270;
    return trois_coins;
  }
  if (nbDiago==0)
    return croix;
}

int Mur::getNbDiago(){
  return tabDiagoMurs[0]+tabDiagoMurs[1]+tabDiagoMurs[2]+tabDiagoMurs[3];
} 

Objet * Mur::getContenant(){
  return NULL;
}
