#ifndef PORTAIL
#define PORTAIL

#include "Case.hpp"
#include "Objet.hpp"

class Portail : public Case{

public:
  Portail(int tailleCase);
  Portail(int tailleCase, int posTableauX, int posTableauY);

  ~Portail();
  
  bool isMur();
  TypeCase getType();
  void afficherTerminal();

  Objet * getContenant();
  
};

#endif
