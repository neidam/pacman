#ifndef PACGUM
#define PACGUM

#include "Objet.hpp"
class Pacgum : public Objet {


public: 
  Pacgum();
  TypeObjet getType();
  int getPoint();
};
#endif
