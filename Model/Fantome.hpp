#ifndef FANTOME
#define FANTOME

#include "Pacman.hpp"

#include "map"

enum EtatFantome{
	blinky, 
	pinky,
	inky,
	clyde,
	poursuite,
	attente,
	sortMaison,
	fuite,
	retourMaison
};


class Fantome : public Entite {
	
	public:

  Fantome(int vitesse, Case * positionCase, std::string nom, Sens direction);
  Fantome(int vitesse, Case* positionCase, Case* caseSortieMaison, std::string type, Sens prochaineDirection, Pacman* pacman);
		~Fantome();
		
		EtatFantome getNomFantome();
		Case* getCaseCible();
		EtatFantome getEtat();		
		
		void setCaseCible(Case* newCaseCible);
		void setEtat(EtatFantome etat);
		
		void setVitesseNormale();
		void setVitesseRalentie();
		
		void definirProchaineDirection();
		void definirCaseCible();
		void deplacer();
		void reinitialiserPos();
		
	private:
		
		int vitesseNormale;
		int vitesseRalentie;
		
		Case* caseCible;
		Case* caseSortieMaison;
		EtatFantome nomFantome;
		Pacman* pacman;
		EtatFantome etat;
		
	
		
};
#endif
