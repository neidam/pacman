#ifndef CASE
#define CASE

#include <iostream>

#include "Objet.hpp"

enum TypeCase {plein, passage, portail, porteFantomes, maisonFantomes, seul, pic, ligne_vide, coin_plein, coin_vide, ligne_plein, double_coin_imcomplet, double_coin_imcomplet_bis, double_coin_vide, coin_complet, double_coin_plein, croix, sablier, trois_coins };

 
class Case {
  
  
  
  int tailleCase;

protected :

  Case* caseGauche;
  Case* caseDroite;  
  Case* caseHaut;
  Case* caseBas;
  int posTableauX;
  int posTableauY;
  int angle;
  bool tabAdjMurs[4];
  bool tabDiagoMurs[4];
  
public:

  Case(int tailleCase);

  Case(int tailleCase, int posTableauX, int posTableauY);
 
  virtual ~Case();

  Case * getCaseGauche();
  Case * getCaseDroite();
  Case * getCaseHaut();
  Case * getCaseBas();
  int getPosTableauX();
  int getPosTableauY();
 
  int getTailleCase();

  void setTailleCase(int taille);
  int getAngle();
  virtual TypeCase getType() = 0;
  virtual bool isMur() = 0;
  virtual void afficherTerminal()=0;
  virtual Objet * getContenant()=0;
  
  void setCaseGauche(Case * c);
  void setCaseDroite(Case * c);
  void setCaseHaut(Case * c);
  void setCaseBas(Case * c);

  void initTabAdjMurs();
  void initTabDiagoMurs();


  bool isMurGauche();
  bool isMurDroite();
  bool isMurHaut();
  bool isMurBas();
  bool isIntersection();
  
  int distanceEntreCases(Case* case2);
 
  bool * getTabAdjMurs();
  bool * getTabDiagoMurs();

  int nbMurAdj();

  void mettreAJour(int tailleCase, int posX, int posY);

  
  
};
  
#endif

