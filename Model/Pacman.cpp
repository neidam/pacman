#include "Pacman.hpp"

using namespace std;

Pacman::Pacman(int vie, int vitesse, Case* positionCase, Sens direction)
	: Entite(vitesse, positionCase, direction )
{
  this->points = 0;
  this->vie = vie;
  this->etat = fuit;
  this->tmpInvincible = 300;
  this->cptInvincible = 0;
}

Pacman::~Pacman(){
}

int Pacman::getPoints(){
  return points;
}

int Pacman::getVie(){
	return vie;
}

int Pacman::getTmpInvincible(){
	return tmpInvincible;
}
 
int Pacman::getCptInvincible(){
	return cptInvincible;
}

EtatPacman Pacman::getEtat(){
	return etat;
}

void Pacman::setPoints(int newPoints){
	points = newPoints;
}

void Pacman::setVie(int newVie){
	vie = newVie;
}

void Pacman::decreaseCptInvincible(){
	cptInvincible--;
	if (cptInvincible <= 0)
		etat = fuit;
}

void Pacman::perdreVie(){
	vie--;
}

void Pacman::deplacer(){
	deplacement();
	
	if(positionCase->getContenant()!=NULL){
		if (positionCase->getContenant()->getType() == bonus){
			etat = invincible;
			cptInvincible = tmpInvincible;
		}
		points += ((Passage*)positionCase)->mangerGum();
	}
}

void Pacman::reinitialiserPos(){
	positionCase = caseDepart;
	avancement = positionCase->getTailleCase() / 2;
	calculPos();
	directionActuelle = directionDepart;
	prochaineDirection = directionDepart;
}

