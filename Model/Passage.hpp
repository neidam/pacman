#ifndef PASSAGE
#define PASSAGE

#include "Case.hpp"

#include "Pacgum.hpp"
#include "Fruit.hpp"
#include "Bonus.hpp"

class Passage : public Case{

  Objet *objet;
  bool verif;
  
public: 

  Passage(int tailleCase);
  Passage(int tailleCase, int posTableauX, int posTableauY);
  Passage(int tailleCase, int posTableauX, int posTableauY, Bonus* bonus);
  Passage(int tailleCase, int posTableauX, int posTableauY, Pacgum* pacgum);
  ~Passage();
  
  bool isMur();
  TypeCase getType();
  Objet * getContenant();

  bool getVerif();
  void resetVerif();
  void afficherTerminal();
  int mangerGum();
  bool isValide();
  bool verifChemin();
  

  void placerFruit(TypeObjet fruit);
  
};

  
#endif
