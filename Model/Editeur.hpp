#ifndef EDITEUR
#define EDITEUR

#include "EtatProgramme.hpp"
#include "../Global.hpp"
#include "Entite.hpp"


enum EtatEditeurCase {placerMur, placerPassage, placerPortail, placerPacman, placerMaisonFantome, sauvegarder};

class Editeur : public EtatProgramme{

  Case * posPacman;

public:
  Editeur();
  
  ~Editeur();
  
  void editerCase(int SDL_x, int SDL_y, EtatEditeurCase etat);

  void ajouterLigneOuColonne(Sens s);
  void supprimerLigne(int num);
  void supprimerColonne(int num);
  int getTailleCasePlateau();
  
  void sauvegarderPlateau();
  void update();
};

#endif
