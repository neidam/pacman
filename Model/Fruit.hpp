#ifndef FRUIT
#define FRUIT

#include "Objet.hpp"

class Fruit : public Objet {

  TypeObjet typeFruit;

  public :
  
  Fruit(TypeObjet type);
    
  TypeObjet getType();
  int getPoint();
  
};


#endif
