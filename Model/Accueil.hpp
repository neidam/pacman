#ifndef ACCUEIL
#define ACCUEIL

#include "EtatProgramme.hpp"

enum EtatAccueil{accueil, jouer, editer};

class Accueil : public EtatProgramme{
  
  EtatAccueil etat;
  

public:
  Accueil();
  
  ~Accueil();
  
  EtatAccueil getEtat();
  void setEtat(EtatAccueil etat);

  void update();
};

#endif
