#ifndef OBJET
#define OBJET

enum TypeObjet{bonus, pacgum, cerise, fraise, orange, pomme, melon, galboss, cloche, cle};

class Objet {
  
public :
  
  virtual TypeObjet getType()=0;
  virtual int getPoint()=0;
  
  
};
#endif
