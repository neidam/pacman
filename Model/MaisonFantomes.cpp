#include "MaisonFantomes.hpp"

using namespace std;

MaisonFantomes::MaisonFantomes(int tailleCase)
  : Case(tailleCase)
{
}

MaisonFantomes::MaisonFantomes(int tailleCase, int posTableauX, int posTableauY)
  : Case(tailleCase, posTableauX, posTableauY)
{
  
}

MaisonFantomes::~MaisonFantomes(){
}

bool MaisonFantomes::isMur(){
  return false;
}

TypeCase MaisonFantomes::getType(){
  return maisonFantomes;
}

Objet * MaisonFantomes::getContenant(){
}

void MaisonFantomes::afficherTerminal(){
    cout<<" ";
}
