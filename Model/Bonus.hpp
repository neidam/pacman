#ifndef BONUS
#define BONUS

#include "Objet.hpp"
class Bonus : public Objet {


public: 
  Bonus();
  TypeObjet getType();
  int getPoint();
};
#endif
