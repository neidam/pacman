all : 
	g++ *.cpp Model/*.cpp View/*.cpp Controller/*.cpp -g -o  Pacman -lSDL2 -lSDL2_image -lSDL2_ttf

clear : 
	rm *.o

test :	all
	valgrind ./Pacman

run : all
	./Pacman
