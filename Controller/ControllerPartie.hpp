#ifndef CONTROLLER_PARTIE
#define CONTROLLER_PARTIE

#include <vector>

#include "../View/AffichageBouton.hpp"
#include "Controller.hpp"
#include "../Model/PartieStandard.hpp"

class ControllerPartie : public Controller{
	
  PartieStandard * partieStandard;
	
public:
  ControllerPartie(PartieStandard * partieStandard);
	
  void gererEvenements();

  void activerBouton(const TypeBouton type);
	
};

#endif
