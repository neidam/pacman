#include "ControllerEditeur.hpp"

#include <iostream>

using namespace std;



ControllerEditeur::ControllerEditeur(Editeur * editeur, vector<AffichageBoutonEdition *> boutonsLargeur, vector<AffichageBoutonEdition *> boutonsHauteur)
  : Controller(){  
  this->editeur = editeur;
  etat = controlEditeur;
  etatEditeurCase = placerMur;
  modif = false;
  boutonEnfonce = NULL;
  this->boutonsHauteur = boutonsHauteur;
  this->boutonsLargeur = boutonsLargeur;
}

void ControllerEditeur::gererEvenements(){
   framerateControl++;
  SDL_PollEvent(event);
  for(map<TypeBouton, AffichageBouton*>::iterator it = mapBoutons.begin(); 
      it != mapBoutons.end(); it++){
    if(it->second->inBouton(event->motion.x, event->motion.y)){
      if (it->second->getEtat() == normal)
	it->second->setEtat(survol);
    }
    else 
      if(it->second->getEtat() == survol)
	it->second->setEtat(normal);
    
  }
  switch (event->type){
    
  case SDL_QUIT:
    etat = fermer;
    break;
    
  case SDL_MOUSEBUTTONDOWN:
    if(framerateControl>10){

      framerateControl=0;
      
      if(isInPlateau(event->motion.x, event->motion.y)){
	editeur->editerCase(event->motion.x,event->motion.y, etatEditeurCase);
	modif = true;
      }
      if(isInListeBoutonEditionH(event->motion.x, event->motion.y)){
	int num = event->motion.y/editeur->getPlateau()->getTailleCase() -1;
      
	if(boutonsHauteur[num]->getType()==imagePlus){
	  if(num==0) editeur->ajouterLigneOuColonne(haut);
	  if(num==boutonsHauteur.size()-1) editeur->ajouterLigneOuColonne(bas);
	  modif = true;
	}
	else {
	  editeur->supprimerLigne(num-1); // -1 Obligatoire pour le lien avec le Plateau
	  modif = true;
	}
	
      }
      if(isInListeBoutonEditionL(event->motion.x, event->motion.y)){
	int num = (event->motion.x - (L_ECRAN - L_JEU))/editeur->getPlateau()->getTailleCase() -1;
	
	if(boutonsLargeur[num]->getType()==imagePlus){
	  if(num==0) editeur->ajouterLigneOuColonne(gauche);
	  if(num==boutonsLargeur.size()-1) editeur->ajouterLigneOuColonne(droite);
	  modif = true;
	}
	else {
	  editeur->supprimerColonne(num-1);
	  modif = true;
	}										 
  }
      for(map<TypeBouton, AffichageBouton*>::iterator it = mapBoutons.begin(); 
	  it != mapBoutons.end(); it++){
	
	if(it->second->inBouton(event->motion.x, event->motion.y)){
	  activerBouton(it->first);
	}
      }
    }
    break;
  }
}

bool ControllerEditeur::getModif(){
  return modif;
}

void ControllerEditeur::resetModif(){
  modif = false;
}

void ControllerEditeur::activerBouton(const TypeBouton type){
  
  switch(type){
    
  case boutonAccueil:
    if (mapBoutons[type]->getEtat() == survol){
      mapBoutons[type]->setEtat(enfonce);
      etat = controlAccueil;
    }
    break;
  case boutonModeMur:
    if (mapBoutons[type]->getEtat() == survol){
      mapBoutons[type]->setEtat(enfonce);
      etatEditeurCase = placerMur;
      if(boutonEnfonce)boutonEnfonce->setEtat(normal);
      boutonEnfonce = mapBoutons[type];
    }
    break;
  case boutonModePassage:
    if (mapBoutons[type]->getEtat() == survol){
      mapBoutons[type]->setEtat(enfonce);
      etatEditeurCase = placerPassage;
      if(boutonEnfonce)boutonEnfonce->setEtat(normal);
      boutonEnfonce = mapBoutons[type];
    }
    break;
  case boutonModePortail:
    if (mapBoutons[type]->getEtat() == survol){
      mapBoutons[type]->setEtat(enfonce);
      etatEditeurCase = placerPortail;
      if(boutonEnfonce)boutonEnfonce->setEtat(normal);
      boutonEnfonce = mapBoutons[type];
    }
    break;
  case boutonModePacman:
    if (mapBoutons[type]->getEtat() == survol){
      mapBoutons[type]->setEtat(enfonce);
      etatEditeurCase = placerPacman;
      if(boutonEnfonce)boutonEnfonce->setEtat(normal);
      boutonEnfonce = mapBoutons[type];
    }
    break;
  case boutonModeFantome:
    if (mapBoutons[type]->getEtat() == survol){
      mapBoutons[type]->setEtat(enfonce);
      etatEditeurCase = placerMaisonFantome;
      if(boutonEnfonce)boutonEnfonce->setEtat(normal);
      boutonEnfonce = mapBoutons[type];
    }
    break;
    
  case boutonSauvegarder:
    if (mapBoutons[type]->getEtat() == survol){
      mapBoutons[type]->setEtat(enfonce);
      etatEditeurCase = sauvegarder;
      if(boutonEnfonce)boutonEnfonce->setEtat(normal);
      boutonEnfonce = mapBoutons[type];
    }
    break;
  }
}

ControllerEditeur::~ControllerEditeur(){
  
}


EtatEditeurCase ControllerEditeur::getEtatEditeur(){
  return etatEditeurCase;
}

void ControllerEditeur::resetEtatEditeur(){
  etatEditeurCase = placerMur;
  boutonEnfonce->setEtat(normal);
  mapBoutons[boutonModeMur]->setEtat(enfonce);
  boutonEnfonce = mapBoutons[boutonModeMur];
}

bool ControllerEditeur::isInPlateau(int x, int y){
  int tailleCase = editeur->getTailleCasePlateau();
  return (x > L_ECRAN - L_JEU + tailleCase*2
	  &&
	  x < L_ECRAN - L_JEU + (editeur->getPlateau()->getLargeur()+2)* tailleCase  
&&
	  y > tailleCase*2
	  &&
	  y < (editeur->getPlateau()->getHauteur()+2)*tailleCase);
  
}

bool ControllerEditeur::isInListeBoutonEditionH(int x, int y){
  int tailleCase = editeur->getTailleCasePlateau();
  return (x> L_ECRAN - L_JEU  && x < L_ECRAN - L_JEU + tailleCase &&
				     y > tailleCase && y < (editeur->getPlateau()->getHauteur()+3)*tailleCase);

}

bool ControllerEditeur::isInListeBoutonEditionL(int x, int y){
  int tailleCase = editeur->getTailleCasePlateau();
  return (x > L_ECRAN - L_JEU + tailleCase && x < L_ECRAN - L_JEU + (editeur->getPlateau()->getLargeur()+3)* tailleCase && y < tailleCase);

}

void ControllerEditeur::setAffichageBoutonsEdition(vector<AffichageBoutonEdition *> boutonsLargeur, vector<AffichageBoutonEdition *> boutonsHauteur){
  this->boutonsLargeur = boutonsLargeur;
  this->boutonsHauteur = boutonsHauteur;
}
