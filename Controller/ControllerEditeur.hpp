#ifndef CONTROLLER_EDITEUR
#define CONTROLLER_EDITEUR

#include "../Model/Editeur.hpp"
#include "../View/AffichageBouton.hpp"
#include "../View/AffichageBoutonEdition.hpp"
#include "Controller.hpp"
#include <vector>



class ControllerEditeur : public Controller{

  Editeur * editeur;
  EtatEditeurCase etatEditeurCase;
  bool modif;
  AffichageBouton * boutonEnfonce;

  std::vector<AffichageBoutonEdition *> boutonsLargeur;
  std::vector<AffichageBoutonEdition *> boutonsHauteur;
  
public :
  ControllerEditeur(Editeur * editeur,  std::vector<AffichageBoutonEdition *> boutonsLargeur, std::vector<AffichageBoutonEdition *> boutonsHauteur);
  
  void gererEvenements();

  void activerBouton(const TypeBouton type);

  bool getModif();
  void resetModif();

  EtatEditeurCase getEtatEditeur();
  
  void resetEtatEditeur();

  void setAffichageBoutonsEdition(std::vector<AffichageBoutonEdition *> boutonsLargeur, std::vector<AffichageBoutonEdition *> boutonsHauteur);
  
  bool isInPlateau(int x, int y);
  bool isInListeBoutonEditionH(int x, int y);
  bool isInListeBoutonEditionL(int x, int y);
  
  ~ControllerEditeur();

};


#endif
