#ifndef CONTROLLER
#define CONTROLLER

#include <SDL2/SDL.h>

#include "../View/AffichageBouton.hpp"

enum EtatController{controlAccueil, controlPartie, controlEditeur, fermer};

class Controller{

protected :
  int framerateControl;
  static SDL_Event * event;
  static EtatController etat;

  std::map<TypeBouton, AffichageBouton *> mapBoutons;

public:
  Controller();
  
  EtatController getEtat();
  
	
  virtual void gererEvenements();

  void ajouterAffichageBouton(const TypeBouton type,  AffichageBouton * affichageBouton);

  virtual void activerBouton(const TypeBouton type);

  static void clearEvent();
  virtual ~Controller();
};

#endif
