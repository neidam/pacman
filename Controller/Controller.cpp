#include "Controller.hpp"

#include <iostream>

using namespace std;

SDL_Event * Controller::event = new SDL_Event();
//SDL_Event * Controller::event = NULL;
EtatController Controller::etat = controlAccueil;

EtatController Controller::getEtat(){
  return etat;
}

Controller::Controller(){
  framerateControl = 0;
}

void Controller::gererEvenements(){}


void Controller::ajouterAffichageBouton(const TypeBouton type,  AffichageBouton * affichageBouton){
  mapBoutons[type] = affichageBouton;
}

void Controller::activerBouton(const TypeBouton type){}

Controller::~Controller(){
}

void Controller::clearEvent(){
  delete event;
}

