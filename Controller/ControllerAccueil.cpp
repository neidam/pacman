#include "ControllerAccueil.hpp"

#include <iostream>

using namespace std;



ControllerAccueil::ControllerAccueil(Accueil * accueil)
  : Controller(){  
  this->accueil = accueil;
  etat = controlAccueil;
}

void ControllerAccueil::gererEvenements(){
 
  SDL_PollEvent(event);
  for(map<TypeBouton, AffichageBouton*>::iterator it = mapBoutons.begin(); 
      it != mapBoutons.end(); it++)
    if(it->second->inBouton(event->motion.x, event->motion.y)){
      if (it->second->getEtat() == normal)
	it->second->setEtat(survol);
    }
    else 
      if(it->second->getEtat() == survol)
	it->second->setEtat(normal);
  
  
  switch (event->type){
    
  case SDL_QUIT:
    etat = fermer;
    break;
    
  case SDL_MOUSEBUTTONDOWN:
    
    for(map<TypeBouton, AffichageBouton*>::iterator it = mapBoutons.begin(); 
	it != mapBoutons.end(); it++){

      if(it->second->inBouton(event->motion.x, event->motion.y)){
	activerBouton(it->first);
      }
    }
    break;
  }
}


void ControllerAccueil::activerBouton(const TypeBouton type){
  
  switch(type){
    
  case boutonJouer:
    if (mapBoutons[type]->getEtat() == survol){  
      accueil->setEtat(jouer);
      etat = controlPartie;
    }
    break;
  case boutonEditeur:
    if (mapBoutons[type]->getEtat() == survol){  
      accueil->setEtat(editer);
      etat = controlEditeur;
    }
    break;
  }
  
}

ControllerAccueil::~ControllerAccueil(){
  
}
