#ifndef CONTROLLER_ACCUEIL
#define CONTROLLER_ACCUEIL

#include "../Model/Accueil.hpp"
#include "../View/AffichageBouton.hpp"
#include "Controller.hpp"

class ControllerAccueil : public Controller{

  Accueil * accueil;
  
public :
  ControllerAccueil(Accueil * accueil);
  
  void gererEvenements();

  void activerBouton(const TypeBouton type);

  ~ControllerAccueil();

};


#endif
